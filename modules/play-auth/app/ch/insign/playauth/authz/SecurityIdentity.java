/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.shiro.util.StringUtils;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Optional;

@Embeddable
public class SecurityIdentity implements Serializable {
    private static final String WILDCARD_CHAR = "*";

    public static final SecurityIdentity ALL = new SecurityIdentity(WILDCARD_CHAR, WILDCARD_CHAR);
    public static final SecurityIdentity UNKNOWN = new SecurityIdentity("?", "?");

    private String type;
    private String identifier;

	@Transient
	private Object source;

    protected SecurityIdentity() {

    }

    public SecurityIdentity(String type, String identifier) {
        if (!StringUtils.hasText(type)) {
            throw new IllegalArgumentException("Type required");
        }
        if (!StringUtils.hasText(identifier)) {
            throw new IllegalArgumentException("Identifier required");
        }
        if (identifier.equals(WILDCARD_CHAR) && !type.equals(WILDCARD_CHAR)) {
            throw new IllegalArgumentException("Class wide SIDs are not allowed");
        }

        this.type = type;
        this.identifier = identifier;
    }

    public SecurityIdentity(Class<?> type, String identifier) {
        if (type == null) {
            throw new IllegalArgumentException("Type required");
        }
        if (!StringUtils.hasText(identifier)) {
            throw new IllegalArgumentException("Identifier required");
        }
        if (identifier.equals(WILDCARD_CHAR)) {
            throw new IllegalArgumentException("Class wide SIDs are not allowed");
        }

        this.type = type.getName();
        this.identifier = identifier;
    }

    public SecurityIdentity(Party party) {
        if (party == null) {
            throw new IllegalArgumentException("Party must not be null");
        }
        if (party.getId() ==  null) {
            throw new IllegalArgumentException("Party.id must not be null");
        }

        type = party.getClass().getName();
        identifier = party.getId();
	    source = party;
    }

    public SecurityIdentity(PartyRole role) {
        if (role == null) {
            throw new IllegalArgumentException("Role cannot be null");
        }
        if (role.getId() ==  null) {
            throw new IllegalArgumentException("Role.id must not be null");
        }


        type = role.getClass().getName();
        identifier = role.getId();
	    source = role;
    }

    public String getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

	public Optional<Object> getSource() {
		return Optional.ofNullable(source);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SecurityIdentity that = (SecurityIdentity) o;

		if (!identifier.equals(that.identifier)) return false;
		if (!type.equals(that.type)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = type.hashCode();
		result = 31 * result + identifier.hashCode();
		return result;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", type)
                .append("identifier", identifier)
                .toString();
    }
}
