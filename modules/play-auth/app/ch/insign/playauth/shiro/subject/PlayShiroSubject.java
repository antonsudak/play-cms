/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.subject;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;

import play.mvc.Http;

public interface PlayShiroSubject extends Subject, ch.insign.playauth.shiro.util.HttpContextSource {

    public static class Builder extends Subject.Builder  {
	private final static Logger logger = LoggerFactory.getLogger(Builder.class);

        public Builder() {
            super(SecurityUtils.getSecurityManager());
        }

        public Builder(SecurityManager securityManager) {
            super(securityManager);
        }

        public Builder(SecurityManager securityManager, Http.Context context) {
            super(securityManager);
            httpContext(context);
        }

        @Override
        protected SubjectContext newSubjectContextInstance() {
            return new ch.insign.playauth.shiro.subject.support.DefaultPlayShiroSubjectContext();
        }

        public Builder httpContext(Http.Context context) {
            if (context != null) {
                ((PlayShiroSubjectContext) getSubjectContext()).setHttpContext(context);
            }
            return this;
        }

        public PlayShiroSubject buildPlayShiroSubject() {
            Subject subject = super.buildSubject();
            if (!(subject instanceof PlayShiroSubject)) {
                String msg = "Subject implementation returned from the SecurityManager was not a " +
                        PlayShiroSubject.class.getName() + " implementation.  Please ensure a Play!-enabled SecurityManager " +
                        "has been configured and made available to this builder.";
                throw new IllegalStateException(msg);
            }
            return (PlayShiroSubject) subject;
        }
    }
}
