/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.utils;

import org.junit.rules.ExternalResource;
import play.db.Database;
import play.db.Databases;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;

public class TestDatabase extends ExternalResource {
    public static final String DATASOURCE_IDENTIFIER = "defaultPersistenceUnit";
    public Database database;
    public JPAApi jpa;

    public void execute(final String sql) {
        database.withConnection(connection -> {
            connection.createStatement().execute(sql);
        });
    }

    @Override
    public void before() {
        database = Databases.inMemoryWith("jndiName", "DefaultDS");
        jpa = JPA.createFor(DATASOURCE_IDENTIFIER);
    }

    @Override
    public void after() {
        jpa.shutdown();
        database.shutdown();
    }
}
