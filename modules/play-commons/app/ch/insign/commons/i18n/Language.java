/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.i18n;

import com.google.inject.Inject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.Application;
import play.Configuration;
import play.i18n.Lang;
import play.mvc.Controller;
import play.mvc.Http;

import javax.inject.Provider;
import java.lang.Exception;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public class Language  {
	private final static Logger logger = LoggerFactory.getLogger(Language.class);

    private static String forcedLang = null;
    @Inject
    private static Configuration configuration;
    @Inject
    private static Provider<Application> applicationProvider;

    private static final String LANGUAGE_DEFAULT = "en";

    private static final String KEY_LANGUAGE_DEFAULT = "cms.defaultLanguage";

    private static final String KEY_LANGUAGE_FALLBACK = "cms.fallbackLanguages";

    /**
     * Returns the default language from the configuration.
     */
    public static String getDefaultLanguage() {
        return configuration.getString(KEY_LANGUAGE_DEFAULT, LANGUAGE_DEFAULT);
    }

    /**
     * Returns the default language from the configuration.
     */
    public static List<String> getFallbackLanguages() {
        return configuration.getStringList(KEY_LANGUAGE_FALLBACK, Collections.singletonList(getDefaultLanguage()));
    }

    /**
     * @return The Language (2 letter abbr) which the user is currently using. If called from a System-Context (Akka), te DefaultCMSLanguage
     */
    public static String getCurrentLanguage() {
        // Workaround if no Http-Context is available (e.g. if Method is called from Unit-Test)
        if (forcedLang!=null) return forcedLang;

        try {
            Http.Context.current();
        } catch(RuntimeException e) {
            // Fake the current language
            return getDefaultLanguage();
        }

        try {
            return Controller.lang().code();
        } catch(Exception e) {
            return getDefaultLanguage();
        }
    }

    /**
     * @return The Lang (play.i18n.Lang) which the user is currently using. If called from a System-Context (Akka), te DefaultCMSLanguage
     */
    public static Lang getCurrentLang() {
        return new Lang(Lang.apply(getCurrentLanguage()));
    }

    /**
     * Get a list of all language codes configured in the project.
     * (Uses Lang.availables() as base input).
     *
     * @return
     */
    public static List<String> getAllLanguages() {
        return Lang.availables(applicationProvider.get()).stream().map(Lang::code).collect(Collectors.toList());
    }

    /**
     * Force-set a current language.
     *
     * Warning: This is meant for tests only (as there is no http context available in tests).
     * Do not use this in your applicationProvider.
     *
     * @param lang language abbrev - or null to switch back to normal language handling
     * @deprecated use an example(PLAY-8) to build tests with custom http context with Play! resources and Mockito
     */
    @Deprecated
    public static void forceCurrentLanguage(String lang) {
        forcedLang = lang;
    }

}
