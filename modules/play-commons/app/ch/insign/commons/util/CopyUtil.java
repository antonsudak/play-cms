/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util;

import ch.insign.commons.db.IgnoreOnCopy;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.sessions.CopyGroup;
import org.springframework.util.ReflectionUtils;
import play.db.jpa.JPA;

public class CopyUtil {

    /**
     * Copy properties from one entity to another ignoring fields marked by {@link IgnoreOnCopy @IgnoreOnCopy}
     * @param src source object
     * @param <T>
     */
    public static <T> T copy(T src) {
        CopyGroup group = new CopyGroup();
        group.setShouldResetPrimaryKey(true);

        T copy = (T) JPA.em().unwrap(JpaEntityManager.class).copy(src, group);

        ReflectionUtils.doWithFields(copy.getClass(),
                field -> {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    field.set(copy, null);
                    field.setAccessible(accessible);
                },
                field -> field.isAnnotationPresent(IgnoreOnCopy.class));

        return copy;
    }
}
