/**
 * mstring js library
 * Timo Schmid <timo.schmid@gmail.com>
 */
var mstring = {
    'show' : function(form, lang) {
        $('li[mstring-tab]', form).removeClass("active");
        $('li[mstring-tab=' + lang + ']', form).addClass("active");
        $('input[name="mstringActiveLang"]', form).val(lang).trigger('change');
        $('div[mstring-lang]', form).each(function(index, item){
            item = $(item);
            if(item.attr("mstring-lang") == lang) {
                item.show();
            } else {
                item.hide();
            }
        });
    },
    'init' : function(defaultLanguage) {
        $('form[mstring-form]').each(function(i, form) {
            mstring.show(form, defaultLanguage);
        });
    }
}

$(function() {
    var regex = /(\?|&)editLang=(\w{2})/g;
    var match = regex.exec(location.href) || [];
    var editLang = match[2] || jsconfig.cms.mstringLanguage;

    mstring.init(editLang);

    $(document).on('click', '.mstring-form-changeTab', function(e){
        e.preventDefault();
        var formId = $(this).data("form_id");
        var lang = $(this).data("lang");
        mstring.show($('form[mstring-form=' + formId + ']'), lang);
    });
});