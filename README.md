Play-CMS
========

The insign Play CMS is meant as a platform for custom Play projects that require solid cms features at their base. Play CMS does not try to be a full-fledged major content management system, however it tries to cover most of today's requirements on content editing.

Documentation and Demo-Application can be found here: [http://play-cms.com](http://play-cms.com)