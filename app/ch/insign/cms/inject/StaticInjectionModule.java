/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.inject;

import ch.insign.cms.actors.AclGarbageCollectorActor;
import ch.insign.cms.blocks.jotformpageblock.actor.SyncFormSubmissions;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.CMS;
import ch.insign.commons.db.Model;
import ch.insign.commons.util.TaskQueue;
import com.google.inject.AbstractModule;

/**
 * @deprecated https://github.com/google/guice/wiki/AvoidStaticState
 */
@Deprecated
public class StaticInjectionModule extends AbstractModule {
    @Override
    protected void configure() {
        requestStaticInjection(CMS.class);
        requestStaticInjection(Model.class);
        requestStaticInjection(BlockFinder.class);
        requestStaticInjection(TaskQueue.class);
        requestStaticInjection(AclGarbageCollectorActor.class);
        requestStaticInjection(SyncFormSubmissions.class);
    }
}
