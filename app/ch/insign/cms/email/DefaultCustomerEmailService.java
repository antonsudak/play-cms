/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.email;

import ch.insign.cms.models.CustomerEmailTemplate;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.party.Party;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

public class DefaultCustomerEmailService implements CustomerEmailService {

    protected final EmailService emailService;

    @Inject
    public DefaultCustomerEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public void send(Party recipient, CustomerEmailTemplate template, HashMap<String, String> data) {
        emailService.sendWithCustomText(
                template.getSubject().get(),
                template.getContent().get(),
                recipient.getEmail(),
                data,
                Language.getCurrentLanguage()
        );
    }

    @Override
    public void send(Party recipient, CustomerEmailTemplate template) {
        send(recipient, template, getDefaultEmailData(recipient));
    }

    @Override
    public void send(Party recipient, List<CustomerEmailTemplate> templates, HashMap<String, String> data) {
        templates.forEach(template -> send(recipient, template, data));
    }

    @Override
    public void send(Party recipient, List<CustomerEmailTemplate> templates) {
        templates.forEach(template -> send(recipient, template));
    }

    protected HashMap<String, String> getDefaultEmailData(Party recipient) {
        HashMap<String, String> data = new HashMap<>();
        data.put("recipient_name", recipient.getName());
        data.put("recipient_email", recipient.getEmail());
        return data;
    }

}
