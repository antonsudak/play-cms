/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.CMSApi;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.events.BlockEventHandlerService;
import ch.insign.cms.models.*;
import ch.insign.cms.permissions.BlockPermission;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.utils.Error;
import ch.insign.cms.views.admin.utils.BackUrl;
import ch.insign.cms.views.html.admin._permissionsPageBlock;
import ch.insign.cms.views.html.admin.deleteBlockInboundLinks;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.commons.i18n.Language;
import ch.insign.commons.util.CopyUtil;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.utils.AuthzFormHandler;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.i18n.MessagesApi;
import play.twirl.api.Html;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.text.Normalizer;
import java.util.*;

import static ch.insign.playauth.PlayAuth.getAccessControlManager;
import static ch.insign.playauth.PlayAuth.getPermissionManager;

/**
 * Controller for all content block related admin actions
 *
 * @author bachi
 *
 */
@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class BlockController extends Controller  {
	private static final Logger logger = LoggerFactory.getLogger(BlockController.class);

    public final String CTXARG_SAVE_BLOCK = "EDITED BLOCK ID";

    private final MessagesApi messagesApi;
    private final CMSApi cmsApi;
    private final BlockEventHandlerService blockEventHandlerService;

    @Inject
    public BlockController(MessagesApi messagesApi, CMSApi cmsApi, BlockEventHandlerService blockEventHandlerService) {
        this.messagesApi = messagesApi;
        this.cmsApi = cmsApi;
        this.blockEventHandlerService = blockEventHandlerService;
    }

    private String getSlug(final String string) {
        String slug = string.toLowerCase();

        // Normalize umlauts
        slug = slug.replaceAll("ä", "ae").replaceAll("ö", "oe").replaceAll("ü", "ue");

        // Normalize all characters and get rid of all diacritical marks (so that e.g. é, ö, à becomes e, o, a).
        slug = Normalizer.normalize(slug, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

        // Replace all remaining non-alphanumeric characters by - and collapse when necessary.
        slug = slug.replaceAll("[^\\p{Alnum}]+", "-");

        return slug;
    }

    public Result addToParent(String blockClass, String target, String slot, String backURL) {

        AbstractBlock targetBlock = AbstractBlock.find.byId(target);
        if (targetBlock == null) {
            return internalServerError("Parent block not found: " + target);
        }

        // Instance access check
        if (!targetBlock.canModify()) {
            return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                    "' tried to add a new " + blockClass + " to " + targetBlock + " without permission.");
        }

        try {
            //TODO: Flash..
            targetBlock.addSubBlock(blockClass);
            return redirect(backURL);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            // TODO: Nice error screens, behaving differently in dev and production etc
            return internalServerError("Could not add new sub block of type '" + blockClass + "' to block " + target);
        }

    }

    public Result add(String blockClass, String key) {
        return TODO;
    }

    /**
     * Display rendered block's permissions partial for given block.
     * @param id Id of the block
     * @param className Class of the block for proper displaying set of permissions, if block not exists yet.
     * @return
     */
    @RequiresBackendAccess
    public Result permissionsBlock(String id, String className) {
        AbstractBlock targetBlock;

        if (id != null && !id.equals("0")) {
            targetBlock = AbstractBlock.find.byId(id);
            if (targetBlock == null) {
                return Error.notFound("Block not found: " + id);
            }

        } else {
            try {
                targetBlock = AbstractBlock.newInstanceOf(className);
            } catch (ClassNotFoundException e) {
                return Error.notFound("Cannot find block class: " + className);
            }
        }
        return ok(_permissionsPageBlock.render(targetBlock));
    }

    /**
     * Save block position after drag and drop
     * @param id
     * @param parentId
     * @param aboveBlockId
     * @return
     */
    public Result savePosition(String id, String parentId, String aboveBlockId) {
        AbstractBlock targetBlock = AbstractBlock.find.byId(id);
        CollectionBlock newParentBlock = CollectionBlock.find.byId(parentId);

        // Instance access check
        if (!targetBlock.canModify() || !newParentBlock.canModify()) {
            return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() + "' tried to move block " +
                    targetBlock + " to parent block " + newParentBlock + " without permission.");
        }

        List<AbstractBlock> subBlocks = newParentBlock.getSubBlocks();

        AbstractBlock aboveBlock = null;
        int aboveBlockPosition = -1;
        if(aboveBlockId != null) {
            aboveBlock = AbstractBlock.find.byId(aboveBlockId);
            aboveBlockPosition = subBlocks.indexOf(aboveBlock);
        }

        // if sorting happens in the same list
        if (subBlocks.indexOf(targetBlock) > -1) {
            //removes targetBlock and get aboveBlock position once more, because it could be changed
            subBlocks.remove(targetBlock);
            if (aboveBlock != null) {
                aboveBlockPosition = subBlocks.indexOf(aboveBlock);
            }
        } else {
            AbstractBlock oldParentBlock = targetBlock.getParentBlock();
            oldParentBlock.getSubBlocks().remove(targetBlock);
            oldParentBlock.save();
        }
        subBlocks.add(aboveBlockPosition + 1, targetBlock);

        targetBlock.setParentBlock(newParentBlock);
        targetBlock.save();
        newParentBlock.markModified();
        newParentBlock.setSubBlocks(subBlocks);
        newParentBlock.save();


        ObjectNode result = Json.newObject();
        result.put("status", "ok");
        return ok(result);
    }

    /**
     * Update (save) an existing block
     * @param id
     * @param backURL
     * @return
     */
    public Result save(String id, String className, String parentId, String backURL) {

        AbstractBlock targetBlock;

        // Save existing
        if (id != null && !id.equals("0")) { // maybe could remove the equals("0") depending on Model
            targetBlock = AbstractBlock.find.byId(id);
            if (targetBlock == null) {
                return internalServerError("Block not found: " + id);
            }

            // Instance access check
            if (!targetBlock.canModify()) {
                return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                        "' tried to save block " + targetBlock + " without permission.");
            }

            // Create new
        } else if (className != null) {
            try {
                targetBlock = AbstractBlock.newInstanceOf(className);
            } catch (ClassNotFoundException e) {
                return internalServerError("Cannot find block class: " + className);
            }

        } else {
            return internalServerError("Cannot save block: No existing id or block class name found");
        }
        // at this point, we have either an existing (attached) or new (detached) block of the proper type.

        // Add the to-be-saved block to the request context
        CmsContext.setCurrent(new CmsContext(CmsContext.Action.SAVE, targetBlock, null));

        // Remove all outbound links first
        // (saving/flushing will add all still existing ones again)
        // Note: Need to do this early, before any prop gets saved by JPA.
        targetBlock.removeOutboundLinks();

	    NavigationItem backItem = NavigationItem.find.byVpath(backURL);

        // Fill in the form data
        @SuppressWarnings("unchecked")
        Form<AbstractBlock> boundForm = (Form<AbstractBlock>) SmartForm.form(targetBlock.getClass());

        String duplicate = boundForm.data().get("duplicate");

        if (duplicate != null && duplicate.equals("true")) {
            return redirect(ch.insign.cms.controllers.routes.BlockController.copy(id, backURL));
        }

        boundForm = boundForm.fill(targetBlock).bindFromRequest();
	    String editLang = Template.nonEmpty(boundForm.field("mstringActiveLang").value(), Language.getDefaultLanguage());
	    CmsContext.current().setLanguage(editLang);

        if (boundForm.hasErrors()) {
            flash("error", messagesApi.get(lang(), "block.save.validate.fail.msg"));
            logger.warn("Form errors found: " + boundForm.errors());
            JPA.em().getTransaction().setRollbackOnly();

            String outputRaw = targetBlock.editForm(boundForm).toString();
            outputRaw = SecureForm.signForms(outputRaw);

            Html output = Html.apply(cmsApi.getFilterManager().processOutput(outputRaw, null));

            return badRequest(output);
        }

        targetBlock = boundForm.get();

        Optional<AbstractBlock> maybeParentBlock =
                Optional.ofNullable(parentId).map(AbstractBlock.find::byId);

        String siteKey = maybeParentBlock.map(AbstractBlock::getSite)
                .filter(StringUtils::isNotBlank)
                .orElseGet(() -> cmsApi.getSites().current().key);

        // Ensure to set the site property before creating nav items as
        // nav items derive site from linked block
        targetBlock.setSite(siteKey);

        // Update / create nav items
        if (targetBlock.getType() == AbstractBlock.BlockType.PAGE) {
            PageBlock pageBlock = (PageBlock) targetBlock;

            // Remember if the backURL points to the item we're editing so we can redirect even if the vpath has changed
            boolean backToEditedPage = false;
            logger.debug("backUrl = " + backURL);
            if (backItem != null && backItem.getPage().getId().equals(pageBlock.getId())) {
                backToEditedPage = true;
            }
            List<String> urls = new ArrayList<>();

            List<String> languages = targetBlock instanceof BackendLinkBlock ? cmsApi.getConfig().allLanguages() : cmsApi.getConfig().frontendLanguages();

            for (String lang : languages) {
                NavigationItem item = pageBlock.createNavItem(lang);
                String vpathNew = boundForm.data().get("vpath." + lang);
                String vpathOld = item.getVirtualPath();
                boolean visible = boundForm.data().containsKey("visible." + lang);
                boolean addToVpathHistory = boundForm.data().containsKey("addToVpathHistory." + lang);
                if (vpathNew != null && !vpathNew.equals(vpathOld)) {
                    try {
                        // Any other navitem of this page got the same vpath?
                        if(urls.contains(vpathNew)) {
                            boundForm.reject("vpath." + lang, messagesApi.get(lang(), "virtual.path.not.available.msg"));
                        }

                        // vpath can't be empty if page for current language is visible
                        if ("".equals(vpathNew) && visible) {
                            boundForm.reject("vpath." + lang, messagesApi.get(lang(), "virtual.path.requiredForVisibleTab.msg"));
                        } else {
                            if ("".equals(vpathNew)) {
                                vpathNew = null;
                            }
                            item.setVirtualPath(vpathNew);
                            urls.add(vpathNew);
                        }
                    } catch (NavigationItem.VpathNotAvailableException e) {
                        boundForm.reject("vpath." + lang, messagesApi.get(lang(), "virtual.path.not.available.msg"));
                    } catch (NavigationItem.VpathNotValidException e) {
                        boundForm.reject("vpath." + lang, messagesApi.get(lang(), "virtual.path.not.valid." + e.getConstraintViolationName() + ".msg"));
                    }

                    // Store the old vpath to history to keep it and redirect when called?
                    if (addToVpathHistory && vpathOld != null) {
                        item.save();
                        item.addVPathHistoryEntry(vpathOld);
                    }
                }
                item.setVisible(visible);
            }
            urls.clear();

            // If we came from the old vpath,
            // then refresh the backURL to a possibly new url
            if (backToEditedPage) {
                backURL = pageBlock.getNavItem(editLang).getURL();
            }
        }

        if (boundForm.hasErrors()) {
            flash("error", messagesApi.get(lang(), "block.save.validate.fail.msg"));
            logger.warn("Form errors found: " + boundForm.errors());

            String outputRaw = targetBlock.editForm(boundForm).toString();
            outputRaw = SecureForm.signForms(outputRaw);

            Html output = Html.apply(cmsApi.getFilterManager().processOutput(outputRaw, null));

            return badRequest(output);
        }

        // Set parent
        if(parentId != null) {
            if (maybeParentBlock.isPresent()) {
                AbstractBlock parentBlock = maybeParentBlock.get();
                parentBlock.getSubBlocks().add(targetBlock);
                targetBlock.setParentBlock(parentBlock);
            } else {
                internalServerError("Cannot find parent to attach to: " + parentId);
            }
        }

        blockEventHandlerService.preSave(targetBlock.getClass(), targetBlock);

        targetBlock.save();
        targetBlock.markModified();

        // Flush here so the LinkContentFilter (@onPersist/@onUpdate) runs while still in the current JPA transaction
        JPA.em().flush();

	    AuthzFormHandler.saveForAllRoles(targetBlock, boundForm.data());

	    blockEventHandlerService.postSave(targetBlock.getClass(), targetBlock);

        String saveAndEdit = boundForm.data().get("saveAndEdit");
        if (saveAndEdit != null && saveAndEdit.equals("true")) {
            return redirect(ch.insign.cms.controllers.routes.BlockController.edit(targetBlock.getId(), backURL, editLang));
        } else {
            return redirect(backURL.replaceAll("editLang=[^&]+","editLang=" + editLang));
        }
    }

    /**
     * Show the edit form for a new block
     *
     * @param blockClass
     * @return
     */
    public Result create(String blockClass, String parentId, String backURL) {
        try {
            AbstractBlock block = AbstractBlock.newInstanceOf(blockClass);
            block.setParentBlock(AbstractBlock.find.byId(parentId));

            // Add the to-be-edited block to the request context
            CmsContext.setCurrent(new CmsContext(CmsContext.Action.EDIT, block, Controller.lang().language()));

            return showEditForm(block, parentId);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return internalServerError("Class not found: " + blockClass);
        }
    }

    /**
     * Show the edit form for a copied block
     *
     * @param id
     * @return
     */
    public Result copy(String id, String backUrl) {
        AbstractBlock originalBlock = AbstractBlock.find.byId(id);
        AbstractBlock block = CopyUtil.copy(originalBlock);
        CmsContext.setCurrent(new CmsContext(CmsContext.Action.EDIT, block, Controller.lang().language()));

        return showEditForm(block, originalBlock.getParentBlock().getId());
    }

    /**
     * Show the edit form for an existing block
     * @param id
     * @param backURL
     * @return
     */
    @RequiresBackendAccess
    public Result edit(String id, String backURL, String editLang) {
        AbstractBlock targetBlock = AbstractBlock.find.byId(id);

        if (targetBlock == null) {
            return internalServerError("Block not found: " + id);
        }

        // Instance access check
        if (!targetBlock.canModify()) {
            return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                    "' tried to edit block " + targetBlock + " without permission.");
        }

        // Add the to-be-edited block to the request context
        CmsContext.setCurrent(new CmsContext(CmsContext.Action.EDIT, targetBlock, editLang));

        return showEditForm(targetBlock, null);
    }

    private Result showEditForm(AbstractBlock targetBlock, String parentId) {
        @SuppressWarnings("unchecked")
        Form<AbstractBlock> editForm = (Form<AbstractBlock>) SmartForm.form(targetBlock.getClass());
        editForm = editForm.fill(targetBlock);
        editForm.data().put("parentId", parentId);

        // Needed to avoid chaining backUrls with the "Save and Edit" button
        String originalUrl = editForm.data().get("originalUrl");
        if (originalUrl == null || originalUrl.equals("")){
        	editForm.data().put("originalUrl", BackUrl.getPrevious());
        }

        if (targetBlock.getType() == AbstractBlock.BlockType.PAGE) {
            PageBlock pageBlock = (PageBlock) targetBlock;
            for (String lang : cmsApi.getConfig().frontendLanguages()) {
                if(pageBlock.getNavItem(lang)!=null) {
                    editForm.data().put("vpath." + lang, pageBlock.getNavItem(lang).getVirtualPath());
                }
            }
        }

        logger.debug(editForm.getClass().getSimpleName());

        Html output = targetBlock.editForm(editForm);

        if (output != null) {
            String outputRaw = SecureForm.signForms(output.toString());
            // Apply filters to the output
            return ok(Html.apply(cmsApi.getFilterManager().processOutput(outputRaw, null)));
        } else {
            return internalServerError(targetBlock.getClass().getSimpleName() + " block class does not provide an edit form.");
        }
    }


    public Result delete(String id, String backURL) {
        AbstractBlock targetBlock = AbstractBlock.find.byId(id);

        if (targetBlock == null) {
            return internalServerError("Block not found: " + id);
        }

        // Instance access check
        if (!targetBlock.canModify()) {
            return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                    "' tried to delete block " + targetBlock + " without permission.");
        }

        // Add the to-be-deleted block to the request context
        CmsContext.setCurrent(new CmsContext(CmsContext.Action.DELETE, targetBlock, null));

        if (targetBlock.getType() == AbstractBlock.BlockType.PAGE || targetBlock.getType() == AbstractBlock.BlockType.BLOCK) {

            // Move to trash bin if configured to use and it can be moved there - otherwise delete (if already in trash)
            if (cmsApi.getConfig().deleteWithoutTrashBin() || !targetBlock.moveToTrash()) {
                // Make sure the backURL does not point to the url of the deleted page, e.g. when removing the page
                // from the frontend the user should be redirected to the index page instead of getting 404 not fund error
                NavigationItem backItem = NavigationItem.find.byVpath(backURL);
                if (backItem != null && backItem.getPage().getId().equals(targetBlock.getId())) {
                    backURL = routes.FrontendController.index().url();
                }

                targetBlock.delete();

                flash("success", messagesApi.get(lang(), "delete.block.success.msg",
                        targetBlock.getClass().getSimpleName(), targetBlock.getId()));
            } else {
                flash("success", messagesApi.get(lang(), "remove.block.success.msg",
                        targetBlock.getClass().getSimpleName(), targetBlock.getId()));
            }
        }

        return redirect(backURL);
    }

    public Result suggestVpath() {
        final String title = request().getQueryString("title");
        final String navItemId = request().getQueryString("nav-item-id");

        final Calendar calendar = Calendar.getInstance();

        // generate virtual path by template
        String vpath = cmsApi.getConfig().vpathTemplate()
                .replaceAll("%p", getSlug(title))
                .replaceAll("%yyyy", String.valueOf(calendar.get(Calendar.YEAR)))
                .replaceAll("%MM", String.valueOf(calendar.get(Calendar.MONTH)))
                .replaceAll("%dd", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));

        // vpath not changed for the navigation item
        if (navItemId != null && navItemId.trim().length() > 0) {
            final NavigationItem navigationItem = NavigationItem.find.byId(navItemId);

            if (navigationItem != null && navigationItem.getVirtualPath() != null
                    && navigationItem.getVirtualPath().equals(vpath)) {
                final ObjectNode result = Json.newObject();
                result.put("vpath", vpath);
                return ok(result);
            }
        }

        // Vpath must be unique: if the suggested path already exists, add an incrementing number at the end
        for (int i = 1; i < 100; i++) {
            if (NavigationItem.find.byVpath(vpath) == null) {
                break;
            }
            vpath += "-" + i;
        }

        final ObjectNode result = Json.newObject();
        result.put("vpath", vpath);

        return ok(result);
    }

    public Result inboundLinks(String id) {
        AbstractBlock targetBlock = AbstractBlock.find.byId(id);

        if (targetBlock == null) {
            return internalServerError("Block not found: " + id);
        }

        if (!targetBlock.canModify()) {
            return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                    "' tried to call inboundLinks() on block " + targetBlock + " without permission.");
        }

        return ok(deleteBlockInboundLinks.render(targetBlock));
    }

    public Result toggleReadRestriction(String id, String backURL) {
        AbstractBlock targetBlock = AbstractBlock.find.byId(id);

        if (targetBlock == null) {
            return Error.notFound("Block not found: " + id);
        }

        if (!targetBlock.canModify()) {
            return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                    "' tried to call updateRestriction() on block " + targetBlock + " without permission.");
        }

	    DomainPermission<?> readPermission = getPermissionManager().applyTarget(BlockPermission.READ, targetBlock);

        if (PlayAuth.isRestricted(readPermission)) {
            getAccessControlManager().allowPermission(SecurityIdentity.ALL, readPermission);
        } else {
            getAccessControlManager().denyPermission(SecurityIdentity.ALL, readPermission);
        }

        return redirect(backURL);
    }
}
