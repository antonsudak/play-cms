/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.CMSApi;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.cms.events.ResetEvent;
import ch.insign.cms.models.Bootstrapper;
import ch.insign.cms.models.Sites;
import ch.insign.cms.permissions.aop.RequiresApplicationMaintenancePermission;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.cms.views.html.admin.searchIndexOverview;
import ch.insign.commons.search.SearchProvider;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.permissions.GlobalDomainPermission;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

/**
 * Holds all actions for setting up or resetting the initial cms data,
 * e.g. default page, trash and draft navigation entries etc.
 */
@Transactional
@With({GlobalActionWrapper.class, CspHeader.class})
public class SetupController extends Controller  {
	private final static Logger logger = LoggerFactory.getLogger(SetupController.class);

    private static volatile boolean resetInProgress = false;

    private final Bootstrapper bootstrapper;
    private CMSApi cmsApi;
    private MessagesApi messagesApi;
    private PlayAuthApi playAuthApi;
    private JPAApi jpaApi;

    @Inject
    public SetupController(Bootstrapper bootstrapper, CMSApi cmsApi, MessagesApi messagesApi, PlayAuthApi playAuthApi, JPAApi jpaApi) {
        this.bootstrapper = bootstrapper;
        this.cmsApi = cmsApi;
        this.messagesApi = messagesApi;
        this.playAuthApi = playAuthApi;
        this.jpaApi = jpaApi;
    }


	/**
     * Resets and initializes all DB data,
     * then adds some example cms data.
     * This is useful during development.
     *
     * BE CAREFUL: All existing data is deleted!
     * Never route to this action in production!
     *
     */
    public synchronized Result reset() {

        // Check permission if only there are some users present
        if (playAuthApi.getPartyManager().findAll().size() > 0) {
            playAuthApi.requirePermission(GlobalDomainPermission.ALL);
        }

        if (cmsApi.getConfig().isResetRouteEnabled()) {
            try {

                // Chrome's prefetch feature triggered this call while typing which lead to race conditions
                if (resetInProgress) {
                    logger.warn("A reset is already in progress - cancelling this one.");
                    return badRequest("A reset is already in progress - cancelling this one.");
                }
                resetInProgress = true;

                ResetEvent.triggerOnBefore();

                jpaApi.withTransaction(() -> {
                    bootstrapper.loadEssentialData();
                    bootstrapper.loadExampleData();
                });

                ResetEvent.triggerOnAfter();

                logger.info("** Reset completed.");
                resetInProgress = false;
                return ok(Html.apply("<a href='/'>Data initialized, now go to /</a>"));

            } catch (Exception e) {
                e.printStackTrace();
                return internalServerError("Error while executing reset - see logs for details.");

            } finally {
                resetInProgress = false;
            }

        } else {
            return internalServerError("Reset is not enabled.");
        }
    }

    /**
     * Adds the initial nodes for a newly added site. Only missing entries are added.
     */
    public synchronized Result initializeNewSite() {
        final Sites.Site site = cmsApi.getSites().current();

        jpaApi.withTransaction(() -> {
            bootstrapper.loadEssentialSiteData(site);
            bootstrapper.loadExampleSiteData(site);
        });

        return redirect(routes.NavigationController.index(null, null));
    }


    /**
     * Admin page for cleaning up the searchindex(es) and scheduling all pages for adding to the new index
     */

    @RequiresBackendAccess
    public Result rebuildSearchIndex() {
        return ok(searchIndexOverview.render());
    }

    /**
     * Empty the searchindex(es) and schedule all pages for adding to the new index
     */

    @RequiresApplicationMaintenancePermission
    public Result doRebuildSearchIndex() {
        if (cmsApi.getSearchManager().getSearchProviders().size() == 0) {
            flash(AdminContext.MESSAGE_ERROR, messagesApi.get(lang(), "admin.searchindex.no_providers"));
        } else {
            cmsApi.getSearchManager().getSearchProviders().forEach(SearchProvider::rebuildIndex);

            flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "admin.searchindex.searchIndex_was_renewed"));
        }

        return redirect(routes.SetupController.rebuildSearchIndex());
    }

    @RequiresBackendAccess
    public Result setupPerformanceMenuItem() {
        doSetupPerformanceMenuItem();
        flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "admin.performance.menu.item.created"));
        return redirect(routes.MetricsController.metrics());
    }

    public static void doSetupPerformanceMenuItem() {
        final String logsGroupKey = "_backend_logs";
        GroupingBlock logsGroup = GroupingBlock.find.byKey(logsGroupKey);
        if (logsGroup != null) {
            final String key = "_backend_performance";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                logsGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang().code(), "/admin/performance");
                page.getNavTitle().set("en", "Performance");
                page.getNavTitle().set("de", "Performance");
                page.setLinkIcon("bar-chart");
                page.save();
            }
        }
    }
}
