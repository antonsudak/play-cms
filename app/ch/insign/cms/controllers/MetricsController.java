/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.permissions.MetricsPermission;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.permissions.aop.RequiresDebugPermission;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.cms.models.Metrics;
import play.Application;
import play.Configuration;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Inject;
import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Transactional
@RequiresBackendAccess
@RequiresDebugPermission
@With(GlobalActionWrapper.class)
public class MetricsController extends Controller {

    private static Date launched = null;

    private final List<String> ALLOWED_TIME_UNITS = Arrays.asList(
        "NANOSECONDS", "MICROSECONDS", "MILLISECONDS", "SECONDS", "MINUTES", "HOURS", "DAYS"
    );
    private final PlayAuthApi authApi;
    private final Metrics metrics;
    private final Configuration configuration;

    @Inject
    public MetricsController(
        PlayAuthApi authApi,
        Metrics metrics,
        Configuration configuration
    ) {
        this.authApi = authApi;
        this.metrics = metrics;
        this.configuration = configuration;
    }

    public Result metrics() {
        authApi.requirePermission(MetricsPermission.BROWSE);

        String durationUnit = configuration.getString("metrics.durationUnit");
        if (!ALLOWED_TIME_UNITS.contains(durationUnit)) {
            durationUnit = "SECONDS";
        }

        String rateUnit = configuration.getString("metrics.rateUnit");
        if (!ALLOWED_TIME_UNITS.contains(rateUnit)) {
            rateUnit = "MINUTES";
        }

        return ok(ch.insign.cms.views.html.admin.performance.show.render(
            metrics.getRegistry().getTimers(),
            TimeUnit.valueOf(durationUnit),
            TimeUnit.valueOf(rateUnit)
        ));
    }

    public Result json() {
        return ok(metrics.reportJson()).as("application/json");
    }

    public Result metricsReset() {
        metrics.init();
        flash(AdminContext.MESSAGE_SUCCESS, "Metrics registry reset.");
        return redirect(ch.insign.cms.controllers.routes.MetricsController.metrics());
    }

    public static Date getLaunched() {
        if (launched == null) {
            final String path = play.Play.application().path().getAbsolutePath() + "/RUNNING_PID";
            final File pidFile = new File(path);
            if (pidFile.exists()) {

                launched = new Date(pidFile.lastModified());
            } else {
                launched = new Date();
            }
        }
        return launched;
    }
}
