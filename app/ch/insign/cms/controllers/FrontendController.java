/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.*;
import ch.insign.cms.utils.Error;
import ch.insign.cms.i18n.LanguageDetector;
import ch.insign.commons.search.SearchQuery;
import ch.insign.commons.search.SearchResult;
import io.vavr.collection.Stream;
import io.vavr.control.Either;
import io.vavr.control.Option;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.Transactional;
import play.mvc.*;
import play.twirl.api.Html;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Contains all actions that render front-end pages.
 *
 * @author bachi
 *
 */
@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class FrontendController extends Controller {

    private static final String KEY_PREF_LANG_FRONTEND ="cms.language.frontend";

    private static final String VPATH_SEPARATOR = "/";

    private static final Logger logger = LoggerFactory.getLogger(FrontendController.class);

    private final LanguageDetector languageDetector;

    @Inject
    public FrontendController(LanguageDetector languageDetector) {
        this.languageDetector = languageDetector;
    }

    /**
     * Show the home page directly or redirect to the page with KEY_HOMEPAGE if its url is not "/"
     */

    public Result index() {

        return siteForRequest(Http.Context.current()).fold(
                // Bail out if we have no current site
                Function.identity(),
                site -> {
                    // TODO: If a vpath with "/"  url exists, redirect on language or not? Initial vs. language selector link? Config..
                    try {
                        String url = PageBlock.find.byKey(PageBlock.KEY_HOMEPAGE).getNavItem().getURL();

                        // If the url of the homepage page is the same (/), then dont redirect
                        if (VPATH_SEPARATOR.equals(url)) {
                            return routeByVpath("");
                        } else {

                            // save flash messages before redirect to another url
                            // because data stored in the Flash scope are available to the next request only.
                            for(Map.Entry<String, String> entry : flash().entrySet()) {
                                flash(entry.getKey(), entry.getValue());
                            }
                            return redirect(url);
                        }
                    } catch (Exception e) {
                        return Error.internal(String.format("Could not find homepage (page block with key '%s' for site '%s')", PageBlock.KEY_HOMEPAGE, site.name));
                    }
                }
        );

    }

    /**
     * Remove trailing slash and do 301-redirect
     *
     * @param path String
     * @return Result
     */
    public Result slashRedirect(String path) {
        return movedPermanently("/" + path);
    }

    /**
     * Generic vpath router. It does look up a vpath, and if found,
     * delegates the call to the navigation item, which in turn delegates
     * the call to the target controller.
     */
    public Result vpath(String vpath) {
        return routeByVpath(vpath);
    }

    public Result routeByVpath(String vpath) {
        String normalizedVpath = normalizeVpath(vpath);
        return siteForRequest(Http.Context.current()).fold(
                Function.identity(),
                site -> showPageForVpath(normalizedVpath)
                        // Vpath not found - try vpath history, setup, then 404
                        .orElse(() -> redirectToMovedPage(normalizedVpath))
                        .orElse(() -> redirectToSetup(normalizedVpath))
                        .getOrElse(vpathNotFound(normalizedVpath))
        );
    }

    private Either<Result, Sites.Site> siteForRequest(Http.Context context) {
        return CMS
                .getSites()
                .forRequest(context.request())
                .map(Either::<Result, Sites.Site>right)
                .orElseGet(() -> Either.left(
                        Error.internal(
                                String.format(
                                        "No site configured for host '%s'!",
                                        context.request().getHeader("Host")
                                )
                        )
                ));
    }

    private String normalizeVpath(String vpath) {
        // Vpaths always are always absolute (start with /)
        return Stream.of(vpath.split(VPATH_SEPARATOR))
                .filter(StringUtils::isNotEmpty)
                .foldLeft(StringUtils.EMPTY, (acc, s) -> acc + VPATH_SEPARATOR + s);
    }

    private Option<NavigationItem> navItemByVpath(String vpath) {
        return Option.of(NavigationItem.find.byVpath(vpath));
    }

    private Option<PageBlock> pageByNavigationItem(NavigationItem navigationItem) {
        return Option.of(navigationItem)
                .map(NavigationItem::getPage);
    }

    private Option<Result> showPageForVpath(String vpath) {
        return navItemByVpath(vpath).flatMap(navigationItem ->
                pageByNavigationItem(navigationItem).map(page -> renderPage(vpath, navigationItem, page))
        );
    }

    private Result renderPage(String vpath, NavigationItem navigationItem, PageBlock page) {
        return languageDetector
                .forNavItem(Http.Context.current(), navigationItem)
                .swap()
                .getOrElse(() -> {
                    // All ok - ask the navItem to route the request
                    logger.info("Vpath '" + vpath + "' routed to " + navigationItem);
                    return page.routeRequest(navigationItem);
                });
    }

    private Option<Result> redirectToMovedPage(String vpath) {
        return Option
                .of(NavigationItem.find.byVpathHistory(vpath))
                .peek(navigationItem ->
                        logger.info("Vpath '" + vpath + "' found in history - redirected (301) to " + navigationItem.getURL())
                )
                .map(NavigationItem::getURL)
                .map(Results::movedPermanently);
    }

    private Option<Result> redirectToSetup(String vpath) {
        if("/".equals(vpath) &&
                CMS.getConfig().isResetRouteEnabled() &&
                AbstractBlock.find.count() == 0) {
            return Option.of(redirect(routes.SetupController.reset()));
        } else {
            return Option.none();
        }
    }

    private Result vpathNotFound(String vpath) {
        return Error.notFound(
                "Could not route vpath '" + vpath + "'. " +
                "Referer: " + request().getHeader("referer")
        );
    }

    /**
     * Renders a cms page with a given nav item id.
     * When using vpaths, this controller should not be addressed directly from the routes,
     * but instead via routes -> FrontendController.vpath() -> navItem.routeRequest() -> FrontendController.page()
     * This allows different NavigationItem .routeRequest() implementations with different controller action
     * mapping but shared vpath usage.
     *
     * @param navId
     * @return
     */
    public Result page(String navId) {
        return renderPage(navId);
    }

    public static Result renderPage(String navId) {

        // Let's assume we have the nav id
        NavigationItem navItem = NavigationItem.find.byId(navId);
        if (navItem == null) {
            return Error.notFound("Could not find a navigation item with id " + navId);
        }

        PageBlock page = navItem.getPage();

        if (page == null) {
            return Error.notFound("Page not found. Nav #" + navId);
        }

        // Add the to-be-shown block to the request context
        CmsContext.setCurrent(new CmsContext(CmsContext.Action.SHOW, page, navItem.getLanguage()));

        // Check the page status (visible/trash/draft)
        // Do this before .canRead() so we can show a 404 instead of a 403 from .canRead()
        if (!page.canModify()) {

            if (page.isTrashed()) {
                return Error.notFound("Page at " + navItem.getURL() + " is in trash. (" + navItem + ").");
            }

            if (page.isDraft()) {
                return Error.notFound("Page at " + navItem.getURL() + " is a draft (" + navItem + ").");
            }

            if (!page.isAlwaysVisibleByVpath()) {
                if (!navItem.isVisible()) {
                    return Error.notFound("Page at " + navItem.getURL() + " is not visible (" + navItem + ").");
                }
            }
        }

        // Access control
        if (!page.canRead()){

            if (!page.checkTimeRestriction()) {
                return Error.unavailable("Page not available due to time restriction: " + page);

            } else {
                return Error.forbidden("Page not available due to access restriction: " + page);
            }
        }

        page.setSelectedNavItem(navItem);
        page.setBaseContext();
        String content = page.cached().toString();

        content = CMS.getFilterManager().processOutput(content, null);
        content = CMS.getUncachedManager().parseContent(content);

        return ok(Html.apply(content));
    }


    /**
     * Search example using the configured SearchProvider.
     * TODO: This is only a proof-of-concept.
     *
     * @param keywords a lucene query
     */
    public Result search(String keywords) {

        SearchQuery query = new SearchQuery(keywords);

        List<SearchResult> results = CMS.getSearchManager().search(query);
        StringBuilder sb = new StringBuilder();

        sb.append("<html>\n<body>\n");
        sb.append(String.valueOf(results.size())).append(" results found for '").append(keywords).append("'<br><br>\n");
        for (SearchResult result : results) {
            sb.append("<p>\n");
            sb.append(String.format("<a href='%s'>%s</a>\n", result.getUrl(), Template.nonEmpty(result.getTitle(), result.getUrl(), "...")));
            sb.append("<br />\n...");
            sb.append(result.getSnippet());
            sb.append("...</p>\n");
        }

        sb.append("</body>\n</html>");

        return ok(Html.apply(sb.toString()));
    }


    /**
     * Generate a sitemap from all pages
     */
    public Result sitemap() {

        // Fail out if we have no current site
        Optional<Sites.Site> site = CMS.getSites().forRequest(request());
        if (!site.isPresent()) {
            return Error.internal(String.format("No site configured for host '%s'!", request().getHeader("Host")));
        }

        List<PageBlock> pages = PageBlock.find.allBySite(site.get().key);
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" +
                " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                " xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" +
                " http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");

        String entry = "<url>\n" +
                "  <loc>%s</loc>\n" +
                "%s" +
                /*"  <changefreq>daily</changefreq>\n" +*/
                /*"  <priority>0.5</priority>\n" +*/
                " </url>";

	    pages.stream()
			    .filter(page -> page.isSearchable() && page.canRead())
			    .flatMap(page -> page.getNavItems().values().stream())
			    .filter(navItem -> navItem.isVisible() && navItem.getURL() != null && !navItem.getURL().isEmpty())
			    .forEach(navItem -> sb.append(String.format(
					    entry,
					    Optional.of(navItem.getURL())
							    .filter(url -> !url.startsWith("/"))
							    .orElseGet(() -> CMS.getConfig().baseUrl() + navItem.getURL()),
					    Optional.ofNullable(navItem.getPage().getModified())
							    .map(Date::toInstant)
							    .map(i -> ZonedDateTime.ofInstant(i, ZoneId.systemDefault()))
							    .map(zdt -> zdt.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
							    .map(when -> "  <lastmod>" + when + "</lastmod>\n").orElse(""))));

        sb.append("\n</urlset>");

        return ok(sb.toString()).as("text/xml");
    }

    /**
     * Minimal robots.txt which points to the sitemap.xml
     */
    public Result robots() {
        String  txt = "User-agent: *\n" +
                "Sitemap: " + CMS.getConfig().baseUrl() + "/sitemap.xml\n";
        return ok(txt);
    }

}
