/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.partyrole.view.PartyRoleCreateView;
import ch.insign.cms.models.partyrole.view.PartyRoleEditView;
import ch.insign.cms.models.partyrole.view.PartyRoleListView;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.support.DefaultPartyRole;
import ch.insign.playauth.permissions.PartyRolePermission;
import ch.insign.playauth.utils.AuthzFormHandler;
import com.google.inject.Inject;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Provider;

import static ch.insign.playauth.PlayAuth.getSecurityIdentity;

@With({GlobalActionWrapper.class, CspHeader.class})
public class RoleController extends Controller {
    private FormFactory formFactory;
    private JPAApi jpaApi;
    private PlayAuthApi authApi;
    private Provider<PartyRoleCreateView> createView;
    private Provider<PartyRoleEditView> editView;
    private Provider<PartyRoleListView> listView;

    @Inject
    public RoleController(
            FormFactory formFactory,
            JPAApi jpaApi,
            PlayAuthApi authApi,
            Provider<PartyRoleCreateView> createView,
            Provider<PartyRoleEditView> editView,
            Provider<PartyRoleListView> listView
    ) {
        this.formFactory = formFactory;
        this.jpaApi = jpaApi;
        this.authApi = authApi;
        this.createView = createView;
        this.editView = editView;
        this.listView = listView;
    }

    @Transactional
    @RequiresAuthentication
    public Result list() {
	    authApi.requirePermission(PartyRolePermission.BROWSE);

        return ok(listView.get().render());
    }

    @Transactional
    @RequiresAuthentication
    public Result edit(String id) {
        DefaultPartyRole role = (DefaultPartyRole) authApi.getPartyRoleManager().find(id);

        if (role == null) {
            return internalServerError("Role not found: " + id);
        }

	    authApi.requirePermission(PartyRolePermission.READ, role);

        Form<DefaultPartyRole> roleForm = SmartForm
                .form(DefaultPartyRole.class)
                .fill(role);

        return ok(SecureForm.signForms(editView.get().setForm(roleForm).setPartyRole(role).render()));
    }

    @Transactional
    @RequiresAuthentication
    public Result doEdit(String id) {
        DefaultPartyRole role = (DefaultPartyRole) authApi.getPartyRoleManager().find(id);

        if (role == null) {
            return internalServerError("Role not found: " + id);
        }

        authApi.requirePermission(PartyRolePermission.EDIT, role);

        Form<DefaultPartyRole> form = SmartForm
                .form(DefaultPartyRole.class)
                .fill(role)
                .bindFromRequest();

        if (form.hasErrors()) {
            if (jpaApi.em().contains(role)) {
                jpaApi.em().refresh(role);
            }
            return badRequest(SecureForm.signForms(editView.get().setForm(form).setPartyRole(role).render()));
        }

        AuthzFormHandler.save(getSecurityIdentity(role), null, form.data());

        save(form.get());

        flash(AdminContext.MESSAGE_SUCCESS, "Role " + form.get().getName() + " has been updated");

        return redirect(routes.RoleController.list());
    }

    @Transactional
    @RequiresAuthentication
    public Result add() {
	    authApi.requirePermission(PartyRolePermission.ADD);

        Form<DefaultPartyRole> form = formFactory.form(DefaultPartyRole.class);
        return ok(SecureForm.signForms(createView.get().setForm(form).render()));
    }

    @Transactional
    @RequiresAuthentication
    public Result doAdd() {
	    authApi.requirePermission(PartyRolePermission.ADD);

        Form<DefaultPartyRole> form = SmartForm.form(DefaultPartyRole.class).bindFromRequest();

        if (authApi.getPartyRoleManager().findOneByName(form.get().getName()) != null) {
            form.reject("name", "Role with such name already exists");
        }

        if (form.hasErrors()) {
            return badRequest(SecureForm.signForms(createView.get().setForm(form).render()));
        }

        DefaultPartyRole role = form.get();
        save(role);

        AuthzFormHandler.save(getSecurityIdentity(role), null, form.data());

        flash(AdminContext.MESSAGE_SUCCESS, "Role " + form.get().getName() + " has been created");

        return redirect(routes.RoleController.list());
    }

    @Transactional
    @RequiresAuthentication
    public Result delete(String id) {
        DefaultPartyRole role = (DefaultPartyRole) authApi.getPartyRoleManager().find(id);

        if (role == null) {
            flash(AdminContext.MESSAGE_ERROR, "Role not found");
        } else {

	        authApi.requirePermission(PartyRolePermission.DELETE, role);

            authApi.getPartyRoleManager().delete(role);
            flash(AdminContext.MESSAGE_SUCCESS, "Role was deleted");
        }

        return redirect(routes.RoleController.list());
    }

    private void save(PartyRole role) {
        if (jpaApi.em().contains(role)) {
            jpaApi.em().merge(role);
        } else {
            jpaApi.em().persist(role);
        }
    }
}
