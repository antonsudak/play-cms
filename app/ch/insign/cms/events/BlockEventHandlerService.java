/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.events;

import ch.insign.cms.models.AbstractBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class BlockEventHandlerService {

    private final BlockEventHandlerProvider eventHandlerProvider;

    @Inject
    public BlockEventHandlerService(BlockEventHandlerProvider eventHandlerProvider) {
        this.eventHandlerProvider = eventHandlerProvider;
    }

    public <T extends AbstractBlock> void preSave(Class<T> clazz, AbstractBlock t) {
        eventHandlerProvider.provideHandler(clazz).ifPresent(h -> preSave(h, clazz, clazz.cast(t)));
    }

    public <T extends AbstractBlock> void postSave(Class<T> clazz, AbstractBlock t) {
        eventHandlerProvider.provideHandler(clazz).ifPresent(h -> postSave(h, clazz, clazz.cast(t)));
    }

    private <T extends AbstractBlock> void preSave(BlockEventHandler<T> handler, Class<T> clazz, T t) {
        handler.preSave(clazz, t);
    }

    private <T extends AbstractBlock> void postSave(BlockEventHandler<T> handler, Class<T> clazz, T t) {
        handler.postSave(clazz, t);
    }

}
