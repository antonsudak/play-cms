/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.i18n;

import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.models.NavigationItem;
import ch.insign.playauth.PlayAuthApi;
import io.vavr.control.Either;
import io.vavr.control.Option;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

public abstract class AbstractLanguageDetector implements LanguageDetector {

    private final PlayAuthApi playAuthApi;

    AbstractLanguageDetector(PlayAuthApi playAuthApi) {
        this.playAuthApi = playAuthApi;
    }

    private static final String QUERY_STRING_LANG = "lang";

    protected Option<String> queryStringLanguage(Http.Context context) {
        return Option.of(context.request().getQueryString(QUERY_STRING_LANG));
    }

    protected Option<Result> languageSpecificRedirect(String language, NavigationItem navigationItem) {
        return Option.of(navigationItem)
                .map(NavigationItem::getPage)
                .map(page -> page.getNavItem(language))
                .map(NavigationItem::getURL)
                .map(Results::redirect);
    }

    protected Either<Result, String> languageSpecificPage(String queryLanguage, NavigationItem navigationItem) {
        // If the user force a language change using ?lang=xx?
        return languageSpecificRedirect(queryLanguage, navigationItem)
                .map(Either::<Result, String>left)
                .getOrElse(() -> Either.right(queryLanguage));
    }

    protected boolean requestLanguageMatchesNavItem(Http.Context context, NavigationItem navigationItem) {
        return context.lang().language().equals(navigationItem.getLanguage());
    }

    protected Either<Result, String> changeLanguage(Http.Context context, NavigationItem navigationItem) {
        // change the user's language to the language of the nav item
        if(!context.changeLang(navigationItem.getLanguage())) {
            throw new IllegalStateException(
                    "The language '" + navigationItem.getLanguage() + "' " +
                            "(NavItem: " + navigationItem.getId() + ")) " +
                            "is not supported."
            );
        }
        // Remember the last used language
        playAuthApi.getCurrentParty().ifPresent(party ->
                party.setPreferredOption(GlobalActionWrapper.KEY_PREF_LANG_FRONTEND, navigationItem.getLanguage())
        );
        return Either.right(navigationItem.getLanguage());
    }

}
