/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.i18n;

import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.models.NavigationItem;
import ch.insign.playauth.PlayAuthApi;
import io.vavr.control.Either;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;

public class DefaultLanguageDetector extends AbstractLanguageDetector {

    @Inject
    public DefaultLanguageDetector(PlayAuthApi playAuthApi) {
        super(playAuthApi);
    }

    @Override
    public Either<Result, String> forNavItem(Http.Context context, NavigationItem navigationItem) {
        if (requestLanguageMatchesNavItem(context, navigationItem)) {
            // When the user's language is the same as the nav item, we're ok
            return Either.right(navigationItem.getLanguage());
        } else {
            return queryStringLanguage(context)
                    .map(queryLanguage -> languageSpecificPage(queryLanguage, navigationItem))
                    .getOrElse(() -> changeLanguage(context, navigationItem));
        }
    }

}
