/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.filters;


import ch.insign.commons.util.Configuration;
import com.google.inject.Inject;
import com.kenshoo.play.metrics.MetricsFilter;
import com.mohiva.play.htmlcompressor.HTMLCompressorFilter;
import com.mohiva.play.xmlcompressor.XMLCompressorFilter;
import play.filters.cors.CORSFilter;
import play.filters.csrf.CSRFFilter;
import play.filters.gzip.GzipFilter;
import play.http.HttpFilters;
import play.mvc.EssentialFilter;

import java.util.ArrayList;
import java.util.List;

public class HtmlFilter implements HttpFilters {

	@Inject
	private CORSFilter corsFilter;

	@Inject
	private XMLCompressorFilter xmlCompressorFilter;

	@Inject
	private HTMLCompressorFilter htmlCompressorFilter;

	@Inject
	private GzipFilter gzipFilter;

	@Inject
	private MetricsFilter metricsFilter;

	@Inject
	private CSRFFilter csrfFilter;

	@Override
	public EssentialFilter[] filters() {
		List<EssentialFilter> filters = new ArrayList<>();

		if (Configuration.getOrElse("filter.htmlCompressor", true)) {
			filters.add(htmlCompressorFilter.asJava());
		}

		if (Configuration.getOrElse("filter.corsFilter", true)) {
			filters.add(corsFilter.asJava());
		}

		if (Configuration.getOrElse("filter.xmlCompressorFilter", true)) {
			filters.add(xmlCompressorFilter.asJava());
		}

		if (Configuration.getOrElse("filter.metrics", true)) {
			filters.add(metricsFilter.asJava());
		}

		if (Configuration.getOrElse("filter.gzip", true)) {
			filters.add(gzipFilter.asJava());
		}

		if (Configuration.getOrElse("filter.csrf", true)) {
			filters.add(csrfFilter.asJava());
		}

		return filters.toArray(new EssentialFilter[filters.size()]);

	}

}
