/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.controllers.CmsContext;
import ch.insign.commons.filter.ContentFilter;
import ch.insign.commons.filter.Filterable;
import ch.insign.commons.util.Configuration;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;
import play.mvc.Controller;
import play.mvc.Http;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Filter content for on-site links.
 * On input, full urls (http://my.domain.ch/my/page) or absolute paths (/my/page) are replaced
 * by a filter tag containing the nav item id.
 *
 * Note: relative paths (../../my/page) are ignored. If using TinyMCE, make sure to set 'relative_urls: false' in tinymce.init()
 *
 * On output, these tags are again converted to the proper link target.
 *
 * @author bachi
 *
 */
public class LinkContentFilter extends ContentFilter  {
	private final static Logger logger = LoggerFactory.getLogger(LinkContentFilter.class);

    private static final Pattern PATTERN = Pattern.compile("href=['\"](.*?)['\"]");
    private static final String TAG = "href";

    protected JPAApi jpaApi;

    @Inject
    public LinkContentFilter (JPAApi jpaApi) {
        this.jpaApi =jpaApi;
    }

    @Override
    public String[] filterTags() {
        return new String[] {TAG};
    }


    @Override
    public String processTagOutput(String tag, List<String> params, Filterable source) {
        if (params.size() > 1) {
            String navId = params.get(1);
            NavigationItem navItem = jpaApi.withTransaction(() -> NavigationItem.find.byId(navId));
            if (navItem != null) {
                Http.Request request = Controller.request();

                final String baseURL = "http" + (request.secure() ? "s" : "") + "://" +
                    Configuration.getOrElse("application.host", request.host());

                return String.format(
                    "href=\"%s\"",
                    Optional.of(navItem.getURL())
                        .filter(url -> !url.startsWith("/"))
                        .orElseGet(() -> baseURL + navItem.getURL())
                );
            } else {
                // If we cannot find a  navItem anymore, we (arguably) return "" to remove the invalid markup part.
                logger.warn("Could not find a navItem for tag: " + tag);
            }
        } else {
            logger.warn("Navigation params array has less then two elements");
        }
        return "";
    }


    @Override
    public String processInput(String input, Filterable source) {

        // Our page's url
        String baseUrl = getBaseUrl();

        Matcher m = PATTERN.matcher(input);

        URL pageURL = null;
        URL hrefURL = null;
        StringBuffer result = new StringBuffer();

        try {
            pageURL = new URL(baseUrl);
        } catch (MalformedURLException e) {
            logger.error("LinkContentFilter: Can't parse page url '" + baseUrl + "', aborting link filtering!", e);
            return null;
        }

        while (m.find()) {
            String href = m.group(1);

            // Build the full url
            try {
                hrefURL = new URL(pageURL, href);
            } catch (MalformedURLException e) {
                logger.warn("LinkContentFilter: Cannot parse url: " + href);
                continue;
            }

            // Ignore external links
            if (!hrefURL.getHost().equalsIgnoreCase(pageURL.getHost())) {
                continue;
            }

            if (hrefURL.getQuery() != null) {
                continue;
            }

            // Try to find a matching nav item
            String path = hrefURL.getPath();
            NavigationItem navItem = jpaApi.withTransaction(() -> NavigationItem.find.byVpath(path));

            if (navItem == null) {
                continue;
            }

            // If found, replace href value with tag: [[href:2234]
            String replacement = String.format("[[%s:%s]]", TAG, navItem.getId());
            logger.debug(String.format("Replacing '%s' with '%s'", m.group(0), replacement));
            m.appendReplacement(result, replacement);


            // Set inbound/outbound link relations (if we have a cms context)
            if (CmsContext.current() !=  null
                    && CmsContext.current().getAction() == CmsContext.Action.SAVE) {

                PageBlock targetPage = navItem.getPage();
                AbstractBlock sourceBlock = CmsContext.current().getBlock();

                if (!sourceBlock.getOutboundLinks().contains(targetPage)) {
                    sourceBlock.getOutboundLinks().add(targetPage);
                    logger.debug(String.format("Adding outbound link %s -> %s", sourceBlock, targetPage));
                    sourceBlock.save();
                }

                if (!targetPage.getInboundLinks().contains(sourceBlock)) {
                    targetPage.getInboundLinks().add(sourceBlock);
                    logger.debug(String.format("Adding inbound link %s -> %s", sourceBlock, targetPage));
                    targetPage.save();
                }
            }
        }



        m.appendTail(result);
        return result.toString();
    }

    private String getBaseUrl() {
        return Configuration.resolveBaseUrl("http://localhost");
    }
}
