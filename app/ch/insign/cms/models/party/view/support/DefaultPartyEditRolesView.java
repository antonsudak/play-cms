/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models.party.view.support;

import ch.insign.cms.models.party.view.PartyEditRolesView;
import ch.insign.cms.models.party.view.PartyMenuItemsView;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.cms.views.html.admin.party.rolesForm;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.support.DefaultParty;
import com.google.inject.Inject;
import play.data.Form;
import play.twirl.api.Html;

import java.util.List;

public class DefaultPartyEditRolesView implements PartyEditRolesView {
    protected final PartyMenuItemsView partyMenuItemsView;
    protected DefaultParty party;
    protected List<PartyRole> roles;
    protected Form partyForm;

    @Inject
    public DefaultPartyEditRolesView(PartyMenuItemsView partyMenuItemsView) {
        this.partyMenuItemsView = partyMenuItemsView;
    }

    @Override
    public DefaultPartyEditRolesView setParty(DefaultParty party) {
        this.party = party;
        return this;
    }

    @Override
    public DefaultPartyEditRolesView setForm(Form partyForm) {
        this.partyForm = partyForm;
        return this;
    }

    @Override
    public PartyEditRolesView setRoles(List<PartyRole> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public Html render() {
        return rolesForm.render(
                new AdminContext(),
                party,
                partyForm,
                roles,
                partyMenuItemsView.setParty(party).render());
    }
}
