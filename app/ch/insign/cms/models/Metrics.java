/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.util.Configuration;
import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.json.MetricsModule;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.elasticsearch.metrics.ElasticsearchReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

/**
 * Provides metric-play default registry access (see https://github.com/kenshoo/metrics-play ),
 * optionally reports to ElasticSearch (see https://github.com/elasticsearch/elasticsearch-metrics-reporter-java )
 * or Jmx (see http://metrics.codahale.com/getting-started/#reporting-via-jmx)
 *
 * @author bachi
 */
@Singleton
public class Metrics  {
    private static final Logger logger = LoggerFactory.getLogger(Metrics.class);

    private Timer pageTime;
    private MetricRegistry registry;

    private ObjectMapper mapper;
    private JmxReporter reporterJmx;

    private com.kenshoo.play.metrics.Metrics metrics;


    @Inject
    public Metrics(com.kenshoo.play.metrics.Metrics metrics) {
        this.metrics = metrics;
        init();
    }

    public Timer getPageTime() {
        return pageTime;
    }

    public MetricRegistry getRegistry() {
        return registry;
    }

    /**
     * Get all metrics as a formatted json string
     */
    public String reportJson() {
        if (registry == null) {
            return "No metric registry available.";
        }
        ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
        StringWriter stringWriter = new StringWriter();
        try {
            writer.writeValue(stringWriter, registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    public void init() {
        mapper = new ObjectMapper().registerModule(new MetricsModule(TimeUnit.MINUTES, TimeUnit.SECONDS, false));
        registry = metrics.defaultRegistry();
        pageTime = registry.timer("cms.page.time");

        // Remove any previously set metrics
        registry.removeMatching((s, metric) -> true);

        if (Configuration.getOrElse("metrics.reportToElastic", false)) {

            String host = Configuration.getOrElse("elastic.host", "localhost:9200")
                    .replace("http://", "")
                    .replace("https://", "");
            int freq = Configuration.getOrElse("metrics.elasticFrequency", 60 * 60);

            String dateFormat = Configuration.getOrElse("metrics.elasticIndexDateFormat", "");

            ElasticsearchReporter reporter;
            try {
                reporter = ElasticsearchReporter.forRegistry(registry)
                        .hosts(host)
                        .indexDateFormat(dateFormat)
                        .build();

                reporter.start(freq, TimeUnit.SECONDS);
                logger.info(String.format("Metrics: Logging to ElasticSearch at %s every %d sec.", host, freq));

            } catch (IOException e) {
                throw new RuntimeException("Metrics: Elastic reporting enabled but cannot access server: " + e.getMessage(), e);
            }
        }

        if (Configuration.getOrElse("metrics.reportToJmx", false)) {
            logger.warn("Metrics: JMX reporting enabled (do not use jmx permanently in production!)");
            if (reporterJmx != null) {
                reporterJmx.stop();
                reporterJmx.close();
            }
            reporterJmx = JmxReporter.forRegistry(registry).build();
            reporterJmx.start();
        }

    }

}
