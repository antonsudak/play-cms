/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.CMSApi;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.cms.blocks.jotformpageblock.permission.JotFormPermission;
import ch.insign.cms.blocks.sliderblock.SliderCollectionBlock;
import ch.insign.cms.blocks.sliderblock.SliderEntryBlock;
import ch.insign.cms.controllers.SetupController;
import ch.insign.cms.permissions.*;
import ch.insign.commons.i18n.Language;
import ch.insign.commons.util.Configuration;
import ch.insign.playauth.authz.AccessControlManager;
import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.PartyRoleManager;
import ch.insign.playauth.party.support.DefaultPartyRole;
import ch.insign.playauth.permissions.GlobalDomainPermission;
import ch.insign.playauth.permissions.PartyPermission;
import ch.insign.playauth.permissions.PartyRolePermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;

import javax.inject.Inject;

public class DefaultBootstrapper implements Bootstrapper {
    private static final Logger logger = LoggerFactory.getLogger(DefaultBootstrapper.class);

    private AccessControlManager accessControlManager;
    private CMSApi cmsApi;
    private PartyRoleManager partyRoleManager;
    private JPAApi jpaApi;

    @Inject
    public DefaultBootstrapper(AccessControlManager accessControlManager, CMSApi cmsApi, PartyRoleManager partyRoleManager, JPAApi jpaApi) {
        this.accessControlManager = accessControlManager;
        this.cmsApi = cmsApi;
        this.partyRoleManager = partyRoleManager;
        this.jpaApi = jpaApi;
    }

    @Override
    synchronized public void loadEssentialData() {
		deleteData();
        createRootNode();
        cmsApi.getSites().getAll().forEach(this::loadEssentialSiteData);
		createBackendNavigation();
        createPartyRoles();
        createParties();
	}

	@Override
    synchronized public void loadExampleData() {
        cmsApi.getSites().getAll().forEach(this::loadExampleSiteData);
    }

    @Override
    synchronized public void loadEssentialSiteData(Sites.Site site) {
        AbstractBlock rootNode = AbstractBlock.find.byKey(PageBlock.KEY_ROOT);

        GroupingBlock siteNode = GroupingBlock.find.byKey(PageBlock.KEY_SITE, site.key);
        if (null == siteNode) {
            siteNode = new GroupingBlock();
            rootNode.addSubBlock(siteNode);

            siteNode.setKey(PageBlock.KEY_SITE);
            siteNode.setSite(site.key);
            siteNode.getNavTitle().set("en", "Site: " + site.name);
            siteNode.getNavTitle().set("de", "Site: " + site.name);
            siteNode.getNavTitle().set("fr", "Site: " + site.name);
            siteNode.save();

            logger.info("Created a new site top node for site '{}'", site.name);
        }

        if (GroupingBlock.find.byKey(PageBlock.KEY_FRONTEND, site.key) == null) {
            logger.info("Creating Frontend node for site '{}'", site.name);

            GroupingBlock frontend = new GroupingBlock();
            siteNode.addSubBlock(frontend);

            frontend.setKey(PageBlock.KEY_FRONTEND);
            frontend.getNavTitle().set("en", "Website");
            frontend.getNavTitle().set("de", "Website");
            frontend.getNavTitle().set("fr", "Website");
            siteNode.save();
        }

        if (GroupingBlock.find.byKey(PageBlock.KEY_DRAFTS, site.key) == null) {
            logger.info("Creating Drafts node for site '{}'",  site.name);

            GroupingBlock drafts = new GroupingBlock();
            siteNode.addSubBlock(drafts);

            drafts.setKey(PageBlock.KEY_DRAFTS);
            drafts.getNavTitle().set("en", "Drafts");
            drafts.getNavTitle().set("de", "Entwürfe");
            drafts.getNavTitle().set("fr", "Brouillons");
            drafts.save();

            // only privileged users can read drafts
            accessControlManager.denyPermission(SecurityIdentity.ALL, BlockPermission.READ, drafts);
        }

        PageBlock systemNode = PageBlock.find.byKey(PageBlock.KEY_SYSTEM, site.key);
        if (systemNode == null) {
            logger.info("Creating System node for site '{}'",  site.name);

            systemNode = new GroupingBlock();
            siteNode.addSubBlock(systemNode);

            systemNode.setKey(PageBlock.KEY_SYSTEM);
            systemNode.getNavTitle().set("en", "System");
            systemNode.getNavTitle().set("de", "System");
            systemNode.getNavTitle().set("fr", "Système");
            systemNode.save();
        }

        if (GroupingBlock.find.byKey(PageBlock.KEY_TRASH, site.key) == null) {
            logger.info("Creating Trash node for site '{}'",  site.name);

            GroupingBlock trash = new GroupingBlock();
            siteNode.addSubBlock(trash);

            trash.setKey(PageBlock.KEY_TRASH);
            trash.getNavTitle().set("en", "Trash bin");
            trash.getNavTitle().set("de", "Mülleimer");
            trash.getNavTitle().set("fr", "Poubelle");
            trash.save();

            // only privileged users can read trash
            accessControlManager.denyPermission(SecurityIdentity.ALL, BlockPermission.READ, trash);
        }

        // Error pages

        if (ErrorPage.find.byKey(ErrorPage.KEY_NOT_FOUND, site.key) == null) {
            ErrorPage page = createErrorPageNotFound(site);
            page.setKey(ErrorPage.KEY_NOT_FOUND);
            systemNode.addSubBlock(page);
            page.save();

            logger.info("Created 404 not found error page: " + page);
        }

        if (ErrorPage.find.byKey(ErrorPage.KEY_UNAVAILABLE, site.key) == null) {
            ErrorPage page = createErrorPageUnavailable(site);
            page.setKey(ErrorPage.KEY_UNAVAILABLE);
            systemNode.addSubBlock(page);
            page.save();

            logger.info("Created 404 unavailable error page: " + page);
        }

        if (ErrorPage.find.byKey(ErrorPage.KEY_INTERNAL_ERROR, site.key) == null) {
            ErrorPage page = createErrorPageInternalServerError(site);
            page.setKey(ErrorPage.KEY_INTERNAL_ERROR);
            systemNode.addSubBlock(page);
            page.save();

            logger.info("Created 500 error page: " + page);
        }

        if (ErrorPage.find.byKey(ErrorPage.KEY_FORBIDDEN, site.key) == null) {
            ErrorPage page = createErrorPageForbidden(site);
            page.setKey(ErrorPage.KEY_FORBIDDEN);
            systemNode.addSubBlock(page);
            page.save();

            logger.info("Created 403 error page: " + page);
        }
    }


    /**
     * Add some example pages and blocks.
     */
    @Override
    synchronized public void loadExampleSiteData(Sites.Site site) {

        // Add the homepage
        // Note: In this example, it's added as root, which will not be shown in the navigation
        GroupingBlock frontendNode = GroupingBlock.find.byKey(PageBlock.KEY_FRONTEND, site.key);

        PageBlock homepage = createPage(PageBlock.KEY_HOMEPAGE, frontendNode, "/home");
        createNavItem(homepage, "/home_de", "de");
        homepage.getNavTitle().set("en", "Home");
        homepage.getPageTitle().set("en", "Home");
        homepage.save();

        // Add 3 pages
        PageBlock page1 = createPage(null, frontendNode, "/page1");
        createNavItem(page1, "/page1_de", "de");
        page1.getNavTitle().set("en", "Page 1");
        page1.getPageTitle().set("en", "Page 1");

        PageBlock page2 = createPage(null, frontendNode, "/page2");
        page2.getNavTitle().set("en", "Page 2");
        page2.getPageTitle().set("en", "Page 2");

        PageBlock page3 = createPage(null, page2, "/page2_1");
        page3.getNavTitle().set("en", "Page 2.1");
        page3.getPageTitle().set("en", "Page 2.1");

        // Add a sub block to the main pane collection
        CollectionBlock mainPane = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "main");
        ContentBlock block, block2, block3, block4;
        try {
            block = (ContentBlock)mainPane.addSubBlock(ContentBlock.class);
            block.getTitle().set("en", "Play! CMS");
            block.getContent().set("en", "The insign Play CMS is meant as a solid platform for custom Play projects " +
                    "that require solid cms features at their base. Play CMS does not try to be a full-fledged major " +
                    "content management system, however it tries to cover most of today's " +
                    "requirements on content editing.");
            block.save();

            block2 = (ContentBlock)mainPane.addSubBlock(ContentBlock.class);
            block2.getTitle().set("en", "Search");
            block2.getContent().set("en", "<p>The CMS offers a flexible interface for fulltext searching. " +
                    "Implementing projects and define their own SearchProviders, or use an existing one " +
                    "or even multiple providers.\n" +
                    "<a href=\"https://confluence.insign.ch/display/PLAY/Search\">Learn more about search</a>.</p>");
            block2.save();

            block3 = (ContentBlock)mainPane.addSubBlock(ContentBlock.class);
            block3.getTitle().set("en", "Caching");
            block3.getContent().set("en", "Play-CMS supports powerful " +
                    "<a href=\"https://confluence.insign.ch/display/PLAY/Caching\">caching mechanisms</a> at the block level. " +
                    "Caching is disabled by default, it can be enabled globally via configuration and refined per block class.");
            block3.save();

            block4 = (ContentBlock)mainPane.addSubBlock(ContentBlock.class);
            block4.getTitle().set("en", "Play Auth");
            block4.getContent().set("en", "<p>The insign <a href=\"https://confluence.insign.ch/display/PLAY/Play+Auth\">Play Auth</a> " +
                    "provides functionality to manage essential information about parties (people and organizations) and " +
                    "about security policies (permissions and roles).</p>\n" +
                    "<p>The PlayAuth also provides methods for authentication, authorization, impersonation and retrieval " +
                    "of a currently authenticated party's info.</p>");
            block4.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Add a sub block to the sidebar collection
        CollectionBlock sidebarPane = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "sidebar");
        ContentBlock sidebarBlock;
        try {
            sidebarBlock = (ContentBlock)sidebarPane.addSubBlock(ContentBlock.class);
            sidebarBlock.getTitle().set("en", "Multi-language");
            sidebarBlock.getContent().set("en", "The cms is " +
                    "<a href=\"https://confluence.insign.ch/display/PLAY/Languages\">multi-language capable</a>. " +
                    "Which means you can easily set content for any page or block " +
                    "for  all available languages. " +
                    "Available languages can be defined for the front- and the backend individually.  ");
            sidebarBlock.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Add a global footer
        ContentBlock footer = new ContentBlock();
        footer.setSite(site.key);
        footer.setKey("GlobalFooter");
        footer.getContent().set("en", "(c) by Example, Inc. This is the site-wide footer.");
        footer.save();

        SliderCollectionBlock sliderCollection = (SliderCollectionBlock) AbstractBlock.find.byKey("slider");
        if (sliderCollection == null) {
            sliderCollection = new SliderCollectionBlock();
            sliderCollection.setSite(site.key);
            sliderCollection.setKey("slider");
            sliderCollection.save();
        }

        try {
            SliderEntryBlock slide1 = (SliderEntryBlock)sliderCollection.addSubBlock(SliderEntryBlock.class);
            slide1.setSite(site.key);
            slide1.getTitle().set("en", "Play! CMS");
            slide1.getText().set("en", "The Play Framework 2 CMS");
            slide1.getLink().set("en", "https://confluence.insign.ch/display/PLAY/Play+CMS");
            slide1.getLinkText().set("en", "Learn more");
            slide1.getTitle().set("en", "Play! CMS");
            slide1.getImage().set("en", "/cms/assets/frontend/img/theme/slides/no-image.jpg");
            slide1.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected PageBlock getNewPageInstance() {
        return new PageBlock();
    }

    private PageBlock createPage(String key, PageBlock parent, String vpathEN) {

        PageBlock page = getNewPageInstance();
        page.setKey(key);
        if (parent != null) {
            parent.getSubBlocks().add(page);
            page.setParentBlock(parent);
        } else {
            logger.warn("Parent is null for new page " + page);
        }
        page.save();

        createNavItem(page, vpathEN, "en");

        return page;
    }

    private NavigationItem createNavItem(PageBlock page, String vpathEN, String langCode) {
        NavigationItem navItem = page.createNavItem(langCode);
        navItem.setVisible(true);

        try {
            navItem.setVirtualPath(vpathEN);
        } catch (NavigationItem.VpathNotAvailableException e) {
            logger.error("loadEssentialData(): Vpath not available: " + vpathEN, e);
        } catch (NavigationItem.VpathNotValidException e) {
            logger.error("loadEssentialData(): Vpath not valid: " + vpathEN, e);
        }

        navItem.save();

        return navItem;
    }

    /**
     * Delete all cms data from the db
     *
     * TODO Try Persistence.generateSchema to clean whole database:
     *
     *    properties.put("javax.persistence.schema-generation.scripts.action", "drop-and-create");
     *    Persistence.generateSchema("default", properties);
     */
    protected void deleteData() {
        logger.warn("** Deleting old CMS data, adding fresh data!");

        // if we are on MySQL (and probably using InnoDB), make sure to disable foreign_key_checks
        if (Configuration.getOrElse("db.default.driver", "").equals("com.mysql.jdbc.Driver")) {
            jpaApi.em().createNativeQuery("SET foreign_key_checks = 0;").executeUpdate();
        } else if (Configuration.getOrElse("db.default.driver", "").equals("org.h2.Driver")) {
            jpaApi.em().createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE;").executeUpdate();
        }

        deleteExampleData();
        deleteEssentialData();

        // if we are on MySQL, re-enable foreign_key_checks
        if (Configuration.getOrElse("db.default.driver", "").equals("com.mysql.jdbc.Driver")) {
            jpaApi.em().createNativeQuery("SET foreign_key_checks = 1;").executeUpdate();
        }  else if (Configuration.getOrElse("db.default.driver", "").equals("org.h2.Driver")) {
            jpaApi.em().createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE;").executeUpdate();
        }

        jpaApi.em().flush();
        jpaApi.em().getEntityManagerFactory().getCache().evictAll();
    }

    protected void deleteEssentialData() {
        jpaApi.em().createQuery("DELETE FROM NavigationItem").executeUpdate();
        jpaApi.em().createQuery("DELETE FROM SearchIndexEntry").executeUpdate();
        jpaApi.em().createQuery("DELETE FROM AbstractBlock").executeUpdate();
        jpaApi.em().createQuery("DELETE FROM MString").executeUpdate();
    }

    protected void deleteExampleData() {

    }

    /**
     * Adds the single root parent node (and returns it) if it doesn't exist yet
     *
     * @return parent root node
     */
    protected AbstractBlock createRootNode() {
        BlockFinder.flush();

        // Add root-level entries
        AbstractBlock root = AbstractBlock.find.rootParent(true);
        if (root != null) {
            return root;
        } else {
            logger.info("Creating the invisible single parent root node.");

            final GroupingBlock newRoot = new GroupingBlock();
            newRoot.setKey(PageBlock.KEY_ROOT);
            newRoot.setSite("*");
            newRoot.save();

            accessControlManager.allowPermission(SecurityIdentity.ALL, BlockPermission.READ, newRoot);

            return newRoot;
        }
    }

    /**
     * Adds the global admin backend (only once, accessible for all sites by default)
     */
    protected void createBackendNavigation() {
        final String lang = Language.getCurrentLanguage();
        final AbstractBlock rootNode = AbstractBlock.find.byKey(PageBlock.KEY_ROOT);

        GroupingBlock backend = GroupingBlock.find.byKey(PageBlock.KEY_BACKEND);
		if (backend == null) {
            logger.info("Creating Backend node.");
            backend = new GroupingBlock();
            rootNode.addSubBlock(backend);
            backend.setKey(PageBlock.KEY_BACKEND);
            backend.setSite("*");
            backend.getNavTitle().set("en", "Admin backend");
            backend.getNavTitle().set("de", "Admin Backend");
            backend.getNavTitle().set("fr", "Admin backend");
			backend.save();

			// only privileged users can read backend
			accessControlManager.denyPermission(SecurityIdentity.ALL, BlockPermission.READ, backend);
		}

        final String userManagementGroupKey = "user_management_group";
        GroupingBlock userManagementGroup = GroupingBlock.find.byKey(userManagementGroupKey);
        if (userManagementGroup == null) {
            userManagementGroup = new GroupingBlock();
            backend.addSubBlock(userManagementGroup);
            userManagementGroup.setKey(userManagementGroupKey);
            userManagementGroup.getNavTitle().set("en", "User Management");
            userManagementGroup.getNavTitle().set("de", "Benutzerverwaltung");
            userManagementGroup.getNavTitle().set("fr", "User Management");
            userManagementGroup.setParentBlock(backend);
            userManagementGroup.setLinkIcon("user");
            userManagementGroup.save();
        }

        {
            final String key = "_backend_usermanagement";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                userManagementGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/users");  // TODO: Put into a common user mgmt folder, e.g. /admin/auth/*
                page.getNavTitle().set("en", "User management");
                page.getNavTitle().set("de", "Benutzerverwaltung");
                page.setLinkIcon("user");
                page.save();
            }
        }

        {
            final String key = "_backend_navigation";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                backend.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/cms/navigation");
                page.getNavTitle().set("en", "Naviation management");
                page.getNavTitle().set("de", "Navigationsverwaltung");
                page.setLinkIcon("sitemap");
                page.save();
            }
        }

        {
            final String key = "_backend_roles";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                userManagementGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/roles");
                page.getNavTitle().set("en", "Roles");
                page.getNavTitle().set("de", "Rollen verwalten");
                page.setLinkIcon("group");
                page.save();
            }
        }

        {
            final String key = "_backend_vpath";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                backend.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/cms/navigation/search");
                page.getNavTitle().set("en", "Virtual paths ");
                page.getNavTitle().set("de", "Virtuelle Pfade verwalten");
                page.setLinkIcon("link");
                page.save();
            }
        }

        final String settingsGroupKey = "_backend_settings";
        GroupingBlock settingsGroup = GroupingBlock.find.byKey(settingsGroupKey);
        if (settingsGroup == null) {
            settingsGroup = new GroupingBlock();
            backend.addSubBlock(settingsGroup);
            settingsGroup.setKey(settingsGroupKey);
            settingsGroup.getNavTitle().set("en", "Settings & Attribute");
            settingsGroup.getNavTitle().set("de", "Settings & Attribute");
            settingsGroup.getNavTitle().set("fr", "Settings & Attribute");
            settingsGroup.setParentBlock(backend);
            settingsGroup.setLinkIcon("wrench");
            settingsGroup.save();
        }

        {
            final String key = "_backend_attributes";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                settingsGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, ch.insign.cms.attributeset.controller.routes.AttributeController.index().url());
                page.getNavTitle().set("en", "Manage Attributes");
                page.getNavTitle().set("de", "Attribute verwalten");
                page.getNavTitle().set("fr", "Manage Attributes");
                page.setLinkIcon("link");
                page.save();
            }
        }

        {
            final String key = "_backend_attribute_sets";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock sets = new BackendLinkBlock();
                settingsGroup.addSubBlock(sets);
                sets.setKey(key);
                sets.getLinkTarget().set(lang, ch.insign.cms.attributeset.controller.routes.AttributeSetController.index().url());
                sets.getNavTitle().set("en", "Manage Attribute Sets");
                sets.getNavTitle().set("de", "Attribute Sets verwalten");
                sets.getNavTitle().set("fr", "Manage Attribute Sets");
                sets.setLinkIcon("link");
                sets.save();
            }
        }

        {
            final String key = "_backend_emails";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                settingsGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/emailtemplate");
                page.getNavTitle().set("de", "E-Mail-Templates");
                page.getNavTitle().set("en", "E-Mail-Templates");
                page.setLinkIcon("envelope");
                page.save();
            }
        }

        {
            final String key = "_backend_customer_emails";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                settingsGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/customerEmailTemplate");
                page.getNavTitle().set("de", "Kunden E-Mail-Templates");
                page.getNavTitle().set("en", "Customer E-Mail-Templates");
                page.setLinkIcon("envelope");
                page.save();
            }
        }

        {
            final String key = "_backend_jotforms";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                settingsGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/jotform/list");
                page.getNavTitle().set("de", "JotForm");
                page.getNavTitle().set("en", "JotForm");
                page.setLinkIcon("list");
                page.save();
            }
        }

        final String logsGroupKey = "_backend_logs";
        GroupingBlock logsGroup = GroupingBlock.find.byKey(logsGroupKey);
        if (logsGroup == null) {
            logsGroup = new GroupingBlock();
            backend.addSubBlock(logsGroup);
            logsGroup.setKey(logsGroupKey);
            logsGroup.getNavTitle().set("en", "Logging");
            logsGroup.getNavTitle().set("de", "Logging");
            logsGroup.setParentBlock(backend);
            logsGroup.setLinkIcon("list");
            logsGroup.save();
        }

        {
            final String key = "_backend_cache";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                logsGroup.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/cache");
                page.getNavTitle().set("en", "Caching");
                page.getNavTitle().set("de", "Caching");
                page.setLinkIcon("refresh");
                page.save();
            }
        }

        SetupController.doSetupPerformanceMenuItem();

        {
            final String key = "_backend_jotforms_submissions";
            if (BackendLinkBlock.find.byKey(key) == null) {
                BackendLinkBlock page = new BackendLinkBlock();
                backend.addSubBlock(page);
                page.setKey(key);
                page.getLinkTarget().set(lang, "/admin/jotform/formSubmissions");
                page.getNavTitle().set("de", "JotForm vorlage");
                page.getNavTitle().set("en", "JotForm submissions");
                page.setLinkIcon("list");
                page.save();
            }
        }

	    if (CMS.getConfig().isResponsiveFileManagerEnable()) {
		    final String key = "_backend_responsive_filemanager";
		    if (BackendLinkBlock.find.byKey(key) == null) {
			    BackendLinkBlock page = new BackendLinkBlock();
			    backend.addSubBlock(page);
			    page.setKey(key);
			    page.getLinkTarget().set(lang, CMS.getConfig().getResponsiveFileManagerUrlPrefix() + "filemanager/dialog.php");
			    page.getNavTitle().set("en", "Filemanager");
			    page.getNavTitle().set("de", "Filemanager");
			    page.setLinkIcon("file-image-o");
			    page.save();
		    }
	    }
    }


	/**
	 * Overwrite this to customize all error pages
	 *
	 * @return
	 */
    protected ErrorPage getErrorPageInstance() {
		return new ErrorPage();
	}

	/**
	 * 404 not found
	 */
    protected ErrorPage createErrorPageNotFound(Sites.Site site) {
		ErrorPage err404 = getErrorPageInstance();
		err404.setSearchable(false);

        err404.getNavTitle().set("en", "Page not found");
        err404.getPageTitle().set("en", "Page not found");
        err404.getNavTitle().set("de", "Seite nicht gefunden");
        err404.getPageTitle().set("de", "Seite nicht gefunden");
        err404.getNavTitle().set("fr", "Page introuvable");
        err404.getPageTitle().set("fr", "Page introuvable");
        try {
            err404.createNavItem("en", "/error/notfound", true).setSite(site.key);
            err404.createNavItem("de", "/fehler/nichtgefunden", true).setSite(site.key);
            err404.createNavItem("fr", "/erreur/pastrouver", true).setSite(site.key);
        } catch (Exception e) {
            logger.warn("Error page url not available" + e);
        }
        return err404;
    }

	/**
	 * 404 no longer available (e.g. sent to trash bin, not in display time)
	 */
    protected ErrorPage createErrorPageUnavailable(Sites.Site site) {
		ErrorPage err404d = getErrorPageInstance();
		err404d.setSearchable(false);

        err404d.getNavTitle().set("en", "Page no longer available");
        err404d.getPageTitle().set("en", "Page no longer available");
        err404d.getNavTitle().set("de", "Seite nicht mehr verfügbar");
        err404d.getPageTitle().set("de", "Seite nicht mehr verfügbar");
        err404d.getNavTitle().set("fr", "Page n'est plus disponible");
        err404d.getPageTitle().set("fr", "Page n'est plus disponible");
        try {
            err404d.createNavItem("en", "/error/unavailable", true).setSite(site.key);
            err404d.createNavItem("de", "/fehler/nichtverfuegbar", true).setSite(site.key);
            err404d.createNavItem("fr", "/erreur/pasdisponible", true).setSite(site.key);
        } catch (Exception e) {
            logger.warn("Error page url not available" + e);
        }
        return err404d;
    }

	/**
	 * 500 server error page
	 */
    protected ErrorPage createErrorPageInternalServerError(Sites.Site site) {
		ErrorPage err500 = getErrorPageInstance();
		err500.setSearchable(false);

        err500.getNavTitle().set("en", "Oops!");
        err500.getPageTitle().set("en", "Oops!");
        err500.getNavTitle().set("de", "Hoppla!");
        err500.getPageTitle().set("de", "Hoppla!");
        err500.getNavTitle().set("fr", "Oups!");
        err500.getPageTitle().set("fr", "Oups!");
        try {
            err500.createNavItem("en", "/error/internalservererror", true).setSite(site.key);
            err500.createNavItem("de", "/fehler/internalservererror", true).setSite(site.key);
            err500.createNavItem("fr", "/erreur/internalservererror", true).setSite(site.key);
        } catch (Exception e) {
            logger.warn("Error page url not available" + e);
        }
        return err500;
    }

    /**
     * 403 server error page
     */
    protected ErrorPage createErrorPageForbidden(Sites.Site site) {
        ErrorPage err403 = getErrorPageInstance();
        err403.setSearchable(false);

        err403.getNavTitle().set("en", "Forbidden");
        err403.getPageTitle().set("en", "Forbidden");
        err403.getNavTitle().set("de", "Forbidden");
        err403.getPageTitle().set("de", "Forbidden");
        err403.getNavTitle().set("fr", "Forbidden");
        err403.getPageTitle().set("fr", "Forbidden");
        try {
            err403.createNavItem("en", "/error/forbidden", true).setSite(site.key);
            err403.createNavItem("de", "/fehler/forbidden", true).setSite(site.key);
            err403.createNavItem("fr", "/erreur/forbidden", true).setSite(site.key);
        } catch (Exception e) {
            logger.warn("Error page url not available" + e);
        }
        return err403;
    }

    /**
     * Create built-in roles & grant them default permissions
     */
    protected void createPartyRoles() {
        // Create roles
        PartyRole superuserRole = partyRoleManager.create(DefaultPartyRole.ROLE_SUPERUSER);
        PartyRole adminRole = partyRoleManager.create(DefaultPartyRole.ROLE_ADMIN);
        PartyRole userRole = partyRoleManager.create(DefaultPartyRole.ROLE_USER);

        // Grant superuser permission
        accessControlManager.allowPermission(superuserRole, GlobalDomainPermission.ALL);

        // Let anonymous parties to read CMS blocks by default
        accessControlManager.allowPermission(SecurityIdentity.ALL, BlockPermission.READ);

        // Application permissions
        accessControlManager.allowPermission(adminRole, ApplicationPermission.BROWSE_BACKEND);
        accessControlManager.allowPermission(adminRole, ApplicationPermission.DEBUG);
        accessControlManager.allowPermission(adminRole, ApplicationPermission.EXECUTE_MAINTENANCE_TASK);

        // Block permissions
        accessControlManager.allowPermission(adminRole, BlockPermission.MODIFY);

        // Error page permission
        accessControlManager.allowPermission(adminRole, ErrorPagePermission.READ_MESSAGE);

        // Navigation permissions
        accessControlManager.allowPermission(adminRole, NavigationPermission.ADD);
        accessControlManager.allowPermission(adminRole, NavigationPermission.BROWSE);
        accessControlManager.allowPermission(adminRole, NavigationPermission.DELETE);
        accessControlManager.allowPermission(adminRole, NavigationPermission.EDIT);
        accessControlManager.allowPermission(adminRole, NavigationPermission.READ);

        // Block permissions
        accessControlManager.allowPermission(adminRole, BlockPermission.MODIFY);

        // Jot form permissions
        accessControlManager.allowPermission(adminRole, JotFormPermission.ADD);
        accessControlManager.allowPermission(adminRole, JotFormPermission.DELETE);
        accessControlManager.allowPermission(adminRole, JotFormPermission.EXPORT_SUBMISSIONS);
        accessControlManager.allowPermission(adminRole, JotFormPermission.VIEW);
        accessControlManager.allowPermission(adminRole, JotFormPermission.VIEW_SUBMISSIONS);

        // Party permissions
        accessControlManager.allowPermission(adminRole, PartyPermission.ADD);
        accessControlManager.allowPermission(adminRole, PartyPermission.BROWSE);
        accessControlManager.allowPermission(adminRole, PartyPermission.IMPERSONATE);
        accessControlManager.allowPermission(adminRole, PartyPermission.DELETE);
        accessControlManager.allowPermission(adminRole, PartyPermission.EDIT);
        accessControlManager.allowPermission(adminRole, PartyPermission.EDIT_PASSWORD);
        accessControlManager.allowPermission(adminRole, PartyPermission.READ);
        accessControlManager.allowPermission(adminRole, PartyPermission.REQUEST_PASSWORD_RESET);

        // Party role permissions
        accessControlManager.allowPermission(adminRole, PartyRolePermission.ADD);
        accessControlManager.allowPermission(adminRole, PartyRolePermission.DELETE);
        accessControlManager.allowPermission(adminRole, PartyRolePermission.BROWSE);
        accessControlManager.allowPermission(adminRole, PartyRolePermission.EDIT);
        accessControlManager.allowPermission(adminRole, PartyRolePermission.GRANT);
        accessControlManager.allowPermission(adminRole, PartyRolePermission.READ);
        accessControlManager.allowPermission(adminRole, PartyRolePermission.REVOKE);

        // Cache permissions
        accessControlManager.allowPermission(adminRole, CachePermission.BROWSE);
        accessControlManager.allowPermission(adminRole, CachePermission.CLEAN);

        // Email Template permissions
        accessControlManager.allowPermission(adminRole, EmailTemplatePermission.ADD);
        accessControlManager.allowPermission(adminRole, EmailTemplatePermission.DELETE);
        accessControlManager.allowPermission(adminRole, EmailTemplatePermission.BROWSE);
        accessControlManager.allowPermission(adminRole, EmailTemplatePermission.EDIT);
        accessControlManager.allowPermission(adminRole, EmailTemplatePermission.EDIT_DESCRIPTION);
        accessControlManager.allowPermission(adminRole, EmailTemplatePermission.READ);

        // Custom Email Template Permission
        accessControlManager.allowPermission(adminRole, CustomerEmailTemplatePermission.ADD);
        accessControlManager.allowPermission(adminRole, CustomerEmailTemplatePermission.BROWSE);
        accessControlManager.allowPermission(adminRole, CustomerEmailTemplatePermission.DELETE);
        accessControlManager.allowPermission(adminRole, CustomerEmailTemplatePermission.EDIT);
        accessControlManager.allowPermission(adminRole, CustomerEmailTemplatePermission.READ);
    }


    protected void createParties() {

    }
}
