/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Controller;
import play.mvc.Http;

import java.util.*;
import java.util.function.Predicate;

/**
 * A project may contain a single or multiple sites.
 * Site settings are immutable and configured, not stored in the db.
 */
public class Sites {
	private static final Logger logger = LoggerFactory.getLogger(Site.class);
	private static final String HOST_WILDCARD = "*";

	/**
	 * The convenient default site configuration.
	 *
	 * The "default" site will be created if there are no sites configured in the application.conf
	 */
	private static final Config DEFAULT_SITE_CONFIG;
	static {
		Map<String, Object> cfg = new HashMap<>();
		cfg.put("name", "default");
		cfg.put("key", "");
		cfg.put("hosts", Collections.singletonList("*"));
		cfg.put("langs.frontend", Arrays.asList("de", "en"));
		cfg.put("langs.backend", Collections.singletonList("en"));
		cfg.put("logoPath", "");

		DEFAULT_SITE_CONFIG = ConfigFactory.parseMap(cfg);
	}


	private Map<String, Site> hostMap = new HashMap<>();
	private Map<String, Site> nameMap = new HashMap<>();
	private Map<String, Site> keyMap = new HashMap<>();

	public static class Site {
		public final String name;
		public final String key;
		public final List<String> hosts;
		public final SiteLangConfig langConfig;
		public final String logoPath;

		/** The full site config entry **/
		public final Config config;

		Site(String name, String key, List<String> hosts, SiteLangConfig languages, String logoPath, Config config) {
			this.name = name;
			this.key = key;
			this.hosts = hosts;
			this.langConfig = languages;
			this.logoPath = logoPath;
			this.config = config;
		}

		/**
		 * Returns the frontend language for a Site
		 */
		public String getFrontendLanguage(Http.Context context) {
			return getLanguageFromOptions(langConfig.frontendLanguageList, context)
					.orElseGet(this::getDefaultFrontendLanguage);
		}

		/**
		 * Returns the frontend language for a Site
		 */
		public String getBackendLanguage(Http.Context context) {
			return getLanguageFromOptions(langConfig.backendLanguageList, context)
					.orElseGet(this::getDefaultBackendLanguage);
		}

		/**
		* Returns the default language for this Site's frontend
		*/
		public String getDefaultFrontendLanguage() {
			return langConfig
					.frontendLanguageList
					.stream()
					.findFirst()
					.orElseThrow(() -> new IllegalArgumentException("No languages defined for site '" + key + "' (frontend)"));
		}

		/**
		 * Returns the default language for this Site's backend
		 */
		public String getDefaultBackendLanguage() {
			return langConfig
					.frontendLanguageList
					.stream()
					.findFirst()
					.orElseThrow(() -> new IllegalArgumentException("No languages defined for site '" + key + "' (backend)"));
		}

		private static Optional<String> getLanguageFromOptions(List<String> alternatives, Http.Context context) {
			return alternatives
					.stream()
					.filter(isLanguageSelected(context))
					.findFirst();
		}

		private static Predicate<String> isLanguageSelected(Http.Context context) {
			return lang -> lang.equalsIgnoreCase(context.lang().code());
		}

	}

	public static class SiteLangConfig {
		public final List<String> frontendLanguageList;
		public final List<String> backendLanguageList;

		SiteLangConfig(List<String> frontendLanguageList, List<String> backendLanguageList) {
			this.frontendLanguageList = frontendLanguageList;
			this.backendLanguageList = backendLanguageList;
		}
	}

	@SuppressWarnings("unchecked")
	public Sites() {
		try {
			List<? extends Config> sitesConfig = ConfigFactory.load().getConfigList("cms.sites");

			if (sitesConfig.isEmpty()) {
				addSite(DEFAULT_SITE_CONFIG);
			} else{
				sitesConfig.forEach(this::addSite);
			}

		} catch (ConfigException.Missing em) {
			// TODO: re-check when finished and link to docs / default entry example
			logger.error("The 'cms.sites' configuration is not valid. Correct it in your application.conf", em);
		}
	}

	@SuppressWarnings("unchecked")
	private void addSite(Config siteConfig) {
		String name = siteConfig.getString("name");
		String key = siteConfig.getString("key");
		String logo = siteConfig.getString("logoPath");
		List<String> hosts = (List<String>) siteConfig.getAnyRefList("hosts");
		List<String> frontendLanguagesList = (List<String>) siteConfig.getAnyRefList("langs.frontend");
		List<String> backendLanguagesList = (List<String>) siteConfig.getAnyRefList("langs.backend");

		SiteLangConfig languageConfig = new SiteLangConfig(frontendLanguagesList, backendLanguagesList);
		Site site = new Site(name, key, hosts, languageConfig, logo, siteConfig);

		addSite(site);
	}

	private void addSite(Site site) {
		nameMap.put(site.name, site);
		keyMap.put(site.key, site);
		for (String host : site.hosts) {
			hostMap.put(host, site);
		}

		logger.info("Added site: '{}' for hosts: '{}'", site.name, String.join(", ", site.hosts));
	}

	/**
	 * Find the site for a given host name.
	 *
	 * - If no matching host name was found, the wildcard host is returned if present (hosts = [*])
	 * - If there's also no wildcard host, Optional.empty is returned - in this case, an error page should be shown.
	 *
	 */
	public Optional<Site> forHost(String host) {

		if (null != host && hostMap.containsKey(host)) {
			return Optional.of(hostMap.get(host));
		}

		if (hostMap.containsKey(HOST_WILDCARD)) {
			return Optional.of(hostMap.get(HOST_WILDCARD));
		}

		logger.warn("No valid site found for host '{}'", host);
		return Optional.empty();
	}

	/**
	 * Find the site for an incoming request (based on its HOST header)
	 * - If no matching host name was found, the wildcard host is returned if present (hosts = [*])
	 * - If there's also no wildcard host, Optional.empty is returned - in this case, an error page should be shown.
	 */
	public Optional<Site> forRequest(Http.Request request) {

		// Browsers add the port usually only if not 80, however some client might not.
		// We only strip 80 so one could run multiple play-cms sites on different ports of the same host
		return forHost(request.host().replaceFirst(":80$", ""));
	}

	/**
	 * Return the site by name, if availble.
	 */
	public Optional<Site> forName(String name) {
		return Optional.ofNullable(nameMap.getOrDefault(name, null));
	}

	/**
	 * Return the site by key, if availble.
	 */
	public Optional<Site> forKey(String key) {
		return Optional.ofNullable(keyMap.get(key));
	}

	/**
	 * Get all configured sites.
	 */
	public Collection<Site> getAll() {
		return nameMap.values();
	}

	/**
	 * Get the Site for the current request.
	 *
	 * Note: If there is no request or no host header, and also no default site (that uses the host wildcard),
	 * a RuntimeException is thrown as we cannot assign a site at all.
	 */
	public Site current() {
		String host;
		try {
			host = Controller.request().host();
		} catch (Exception e) {
			logger.debug("No context available while getting current site. Trying to use default site. ");
			host = null;
		}

		Optional<Site> site = forHost(host);

		if (site.isPresent()) {
			return site.get();
		} else {
			throw new RuntimeException("No site configured for current request host: " + host);
		}
	}

	/**
	 * Parse current site for request and retrieve its logo path
	 * @return - site logo's path or null if not presents
	 */
	public String getLogoPathForCurrentSite() {
		Site currentSiteInstance = current();

		return currentSiteInstance.logoPath;
	}

}
