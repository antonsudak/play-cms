/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.controllers.FrontendController;
import ch.insign.cms.views.html.admin.pageBlockEdit;
import ch.insign.cms.views.html.pageBlock;
import ch.insign.commons.db.IgnoreOnCopy;
import ch.insign.commons.db.MString;
import ch.insign.commons.i18n.Language;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.format.Formats;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.twirl.api.Html;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "cms_block_page")
@DiscriminatorValue("PageBlock")
@NamedQueries(value = {
        @NamedQuery(
                name = "Block.findAllBySite",
                query = "SELECT b FROM PageBlock b WHERE b.site = :site"),
})
public class PageBlock extends AbstractBlock  implements Page {
    private final static Logger logger = LoggerFactory.getLogger(PageBlock.class);

    public static BlockFinder<PageBlock> find = new BlockFinder<>(PageBlock.class);

    public final static String KEY_ROOT = "_root";
    public static final String KEY_SITE = "_site";
    public final static String KEY_FRONTEND = "_frontend";
    public final static String KEY_BACKEND = "_backend";
    public final static String KEY_DRAFTS = "_drafts";
    public final static String KEY_SYSTEM = "_system";
    public final static String KEY_TRASH = "_trash";
    public static final String KEY_HOMEPAGE = "homepage";

    public static final List<String> SYSTEM_KEYS = Arrays.asList(
            PageBlock.KEY_BACKEND,
            PageBlock.KEY_FRONTEND,
            PageBlock.KEY_SYSTEM,
            PageBlock.KEY_DRAFTS,
            PageBlock.KEY_TRASH,
            PageBlock.KEY_SITE,
            PageBlock.KEY_BACKEND);

    @MString.MStringRequiredForVisibleTab
    @OneToOne (cascade=CascadeType.ALL)
    private MString navTitle = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString metaTitle = new MString();

    @MString.MStringRequiredForVisibleTab
    @OneToOne (cascade=CascadeType.ALL)
    private MString pageTitle = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString pageDescription = new MString();

    @OneToMany(mappedBy="page", cascade=CascadeType.ALL)
    @MapKey(name="language")
    @IgnoreOnCopy
    private Map<String, NavigationItem> navItems = new HashMap<>();

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date publishDate;

    private boolean searchable = true;

    /** Used to temporarily store the nav item that was clicked, which led to this page **/
    @Transient
    private NavigationItem activeNavItem;


    public MString getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(MString metaTitle) {
        this.metaTitle = metaTitle;
    }

    public MString getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(MString title) {
        this.pageTitle = title;
    }

    public MString getNavTitle() {
        return navTitle;
    }

    public void setNavTitle(MString title) {
        this.navTitle = title;
    }

    public MString getPageDescription() {
        return pageDescription;
    }

    public void setPageDescription(MString description) {
        this.pageDescription = description;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    /**
     * Get all sub-pages.
     */
    public List<PageBlock> getSubPages() {
        List<PageBlock> pages = new ArrayList<>();
        for (AbstractBlock block : getSubBlocks()) {
            if (block instanceof PageBlock) pages.add((PageBlock) block);
        }
        return pages;
    }

    public boolean isAlwaysVisibleByVpath() {
        return false;
    }

    /**
     * Get the page's url for the selected language if available (and visible). Otherwise returns the home page
     * in the selected language.
     *
     * @return virtual path
     */
    @Override
    public String alternateLanguageUrl(String lang) {
        if(getNavItem(lang) != null && getNavItem(lang).isVisible() && getNavItem(lang).getVirtualPath() != null) {
            return uriWithParameters(getNavItem(lang).getVirtualPath(), Http.Context.current().request().queryString());

        } else {
            try {
                return PageBlock.find.byKey(KEY_HOMEPAGE).getNavItem(lang).getURL();

            } catch (Exception e) {
                logger.error("alternateLanguageUrl: Error while trying to find homepage with key " + KEY_HOMEPAGE + " in language: " + lang + ". This means your setup is not correct.", e);
                return "/"; // return a fail-safe url
            }
        }
    }

    /**
     * Add GET parameters to the uri
     */
    private String uriWithParameters(String uri, Map<String, String[]> parameters) {
        if (parameters.size() == 0) {
            return uri;
        }

        List<NameValuePair> params = new LinkedList<>();
        for (Map.Entry<String,String[]> entry : parameters.entrySet()) {
            params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()[0]));
        }

        uri += (uri.indexOf('?') == -1) ? "?" : "&";
        uri += URLEncodedUtils.format(params, "utf-8");

        return uri;
    }

    public Map<String, NavigationItem> getNavItems() {
        return navItems;
    }

    /**
     * Check if the block has a parent. Log an error if it has not.
     *
     * (Previously, we've set the root node as parent if a parent was missing. If the block subsequently has set one,
     * the link to the root node however remained.)
     */
    @PrePersist
    @PreUpdate
    protected void onSave() {
        if (getParentBlock() == null && !KEY_ROOT.equals(getKey())) {
            logger.error("Saving block '{}' without a parent node - if none is subsequently set, this block will be an orphan.", this);
        }
    }

    @Override
    public void save() {
        setDefaultNavTitle();
        super.save();
    }

    /**
     * Set default value for empty navTitle fields based on first non empty one
     *
     */
    private void setDefaultNavTitle() {
        List<String> allLangs = CMS.getConfig().frontendLanguages();

        if (allLangs.size() > 1) {
            allLangs
                .stream()
                .map(lang -> getNavTitle().get(lang))
                // Get first non-empty navTitle
                .filter(val -> !StringUtils.isEmpty(val))
                .findFirst()
                .ifPresent(
                    // Set found navTitle for all empty navTitles
                    navTitle -> allLangs
                        .stream()
                        .filter(lang -> StringUtils.isEmpty(getNavTitle().get(lang)))
                        .forEach(lang -> getNavTitle().set(lang, navTitle))
                );
        }
    }

    /**
     * Returns always a nav item.
     *
     * Get the nav item for the currently selected language, or, if not available,
     * for the fallback language, or if not available the first we have.
     * If we have none, create one for the current language.
     *
     * Note: This method can be safely used in front-end templates as it wont return null.
     *
     * @return a nav item - never null
     */
    @Nonnull
    public NavigationItem getNavItem() {
        if (navItems.containsKey(Language.getCurrentLanguage())) {
            return navItems.get(Language.getCurrentLanguage());
        }

        if (navItems.containsKey(Language.getDefaultLanguage())) {
            return navItems.get(Language.getDefaultLanguage());
        }

        if (navItems.size()>0) {
            return (NavigationItem) navItems.values().toArray()[0];
        }

        return createNavItem(Language.getCurrentLanguage());
    }

    /**
     * Get the existing nav item for the selected language
     * @return the nav item, or null if not available for the required language.
     */
    @Nullable
    public NavigationItem getNavItem(String language) {
        if (navItems != null) {
            return navItems.get(language);
        }

        return null;
    }

    public void setSelectedNavItem(NavigationItem activeNavItem) {
        this.activeNavItem = activeNavItem;
    }

    public NavigationItem getSelectedNavItem() {
        return activeNavItem;
    }

    /**
     * Is the current page the one called in this request?
     */
    public boolean isActive() {
        return (activeNavItem != null && activeNavItem.getPage().equals(this));
    }


    /**
     * Mark the current page as active.
     * (does not check if others are marked active, too)
     */
    public boolean isAnySubBlockActive() {
        for(AbstractBlock subBlock: getSubPages()){
            if(subBlock instanceof PageBlock && ((PageBlock) subBlock).isActive()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Mark the current page as active.
     * (does not check if others are marked active, too)
     */
    public void setActive(boolean active) {
        if (active) {
            activeNavItem = getNavItem();
        } else {
            activeNavItem = null;
        }
    }


    @Override
    public AbstractBlock.BlockType getType() {
        return BlockType.PAGE;
    }


    @Override
    public Html render() {
	    return pageBlock.render(this);
    }

    /**
     * Used in {@link ch.insign.cms.blocks.parametrizedcollection.ParametrizedCollectionBlock} to render preview
     * @return html preview presentation of this block
     */
    public Html renderPreview() {
        return render();
    }

    @Override
    public Html editForm(Form editForm) {
        return pageBlockEdit.render(this, editForm, Controller.request().getQueryString("backURL"), null, null, null);
    }


    /**
     * Creates a new nav entry for the selected language - or returns an existing one if it already
     * exists for this language.
     *
     * @param language
     * @return
     */
    public NavigationItem createNavItem(String language) {
        if (language == null || language.equals("")) {
            throw new IllegalArgumentException("createNavItem called with no language for " + this);
        }
        NavigationItem item = getNavItem(language);
        if (item == null)  {
            item = new NavigationItem();
            item.setSite(getSite());
            item.setLanguage(language);
            getNavItems().put(language, item);
            item.setPage(this);
        }
        return item;
    }

    /**
     * Create and configure a new nav entry. If one already exists for this language, it will be overridden.
     *
     * @param language
     * @param vpath
     * @param visible
     * @return the created navigation  item
     * @throws NavigationItem.VpathNotAvailableException
     * @throws NavigationItem.VpathNotValidException
     */
    public NavigationItem createNavItem(String language, String vpath, boolean visible) throws NavigationItem.VpathNotAvailableException, NavigationItem.VpathNotValidException {
        NavigationItem item = createNavItem(language);
        item.setVirtualPath(vpath);
        item.setVisible(visible);
        return item;
    }

    /**
     * Returns whether the page is visible in at least one language.
     * This means, if navItems are still set to invisible but at least one to visible,
     * the page is marked as visible.
     */
    public boolean isVisible() {
        for (NavigationItem navItem : getNavItems().values()) {
            if (navItem.isVisible()) return true;
        }
        return false;
    }

    /**
     * Deletgate the generic (/*) vpath controller's call to our concrete Controller action.
     * Note: generic user- and time / visible / trash / draft restrictions are checked in the FrontendController.
     *
     * Overwrite this to route requests to specific controllers.
     *
     * @param navItem the nav item that the generic router found
     * @return the Result object of this call
     */
    public Result routeRequest(NavigationItem navItem) {
        return FrontendController.renderPage(navItem.getId());
    }

    /**
     * This handler lets you intercept url requests and define your own url (based on the selected nav item)     * (Beware of
     * @param navItem The item for which to return a url
     * @return an optional url (/some/local/path or http://www.fullexternal.com/some/path)
     */
    public Optional<String> onGetUrl(NavigationItem navItem) {
        return Optional.empty();
    }

    public String toString() {
        return String.format(getClass().getSimpleName() + " (id: %s, title: %s, key: %s)", getId(), Template.nonEmpty(getPageTitle(), getNavTitle()), getKey());
    }

    /**
     * Indicates whether this page is searchable (and should be added to a search index if any is used)
     * Default is true
     */
    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    /**
     * Return true if this page can be added to a search index
     */
    public boolean isAvailableForSearch() {
        return searchable && isVisible();
    }

    /**
     * If a page is moved to the trash, set all versions (nav items) to invisible.
     * Like this, it will remain in search indices, but wont be found/shown.
     *
     */
    @Override
    public boolean moveToTrash() {
        boolean result = super.moveToTrash();

        if (result) {
            for (NavigationItem item : navItems.values()) {
                item.setVisible(false);
            }
        }
        return result;
    }

    public boolean isSystemBlock() {
        return SYSTEM_KEYS.contains(getKey());
    }
}
