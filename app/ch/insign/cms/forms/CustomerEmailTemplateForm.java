/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.forms;

import ch.insign.cms.models.CustomerEmailTemplate;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerEmailTemplateForm {

    @Transient
    private boolean sendEmail = false;

    @Transient
    private List<CustomerEmailTemplate> customerEmailTemplates = new ArrayList<>();

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public List<CustomerEmailTemplate> getCustomerEmailTemplates() {
        return customerEmailTemplates;
    }

    public void setCustomerEmailTemplates(List<CustomerEmailTemplate> customerEmailTemplates) {
        this.customerEmailTemplates = customerEmailTemplates;
    }

    public Map<String, List<ValidationError>> validate() {
        Map<String, List<ValidationError>> errors = new HashMap<>();

        if(sendEmail && this.customerEmailTemplates.isEmpty()) {
            List<ValidationError> list = new ArrayList<>();
            list.add(new ValidationError("customerEmailTemplates", Messages.get("backend.user.error.email.required")));
            errors.put("customerEmailTemplates", list);
        }

        return errors.size() > 0 ? errors :  null;
    }
}
