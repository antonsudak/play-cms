/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import ch.insign.commons.db.MString;
import ch.insign.commons.db.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a single element in a set of possible values. It is intended to represent
 * an element in an HTML select field (so, a dropdown with possible values). See {@link OptionAttribute}
 * for the implementation.
 */
@Entity
@Table(name="cms_eav_option")
public class Option extends Model {
    private final static Logger logger = LoggerFactory.getLogger(Option.class);

    public static final Model.Finder<Option> find = new Model.Finder<>(Option.class);
    
    @OneToOne(cascade = CascadeType.ALL)
    private MString name;

    @OneToOne (cascade=CascadeType.ALL)
    private MString synonyms = new MString();

    @ManyToOne
    private OptionAttribute parentAttribute;

    @ManyToMany(mappedBy = "value")
    private List<OptionValue> mappedValues = new ArrayList<>();

    @ManyToOne
    private Option parentOption;

    @OneToMany(mappedBy = "parentOption", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderColumn
    private List<Option> children = new ArrayList<>();

    @Override
    public void delete() {
        if (parentAttribute != null){
            parentAttribute.getOptions().remove(this);
            parentAttribute.save();
        }
        if (parentOption != null){
            parentOption.getChildren().remove(this);
            parentOption.save();
        }
        super.delete();
    }

    // ======= Getters and Setters ========

    public List<Option> getChildren() {
        return children;
    }

    public Attribute getParentAttribute() {
        return parentAttribute;
    }

    public void setParentAttribute(OptionAttribute parentAttribute) {
        this.parentAttribute = parentAttribute;
    }

    public MString getName() {
        return name;
    }

    public void setName(MString name){
        this.name = name;
    }

    public void setParentOption(Option parentOption) {
        this.parentOption = parentOption;
    }

    public Option getParentOption() {
        return parentOption;
    }

    public List<OptionValue> getMappedValues() {
        return mappedValues;
    }

    public void setMappedValues(List<OptionValue> mappedValues) {
        this.mappedValues = mappedValues;
    }

    public void addMappedValue(OptionValue value) {
        mappedValues.add(value);
    }

    public MString getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(MString synonyms) {
        this.synonyms = synonyms;
    }

}
