/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import ch.insign.cms.attributeset.views.html.backend.attributeAddEdit;
import ch.insign.commons.db.MString;
import ch.insign.commons.db.Model;
import ch.insign.commons.db.SmartForm;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Html;

import javax.persistence.*;

@Entity
@Table(name="cms_eav_attribute")
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name="attribute_type")
public abstract class Attribute extends Model {

    public static final AttributeFinder find = new AttributeFinder();

    @Column(name = "attr_key")
    private String key;

    @OneToOne(cascade= CascadeType.ALL)
    private MString name;

    /**
     * This method is called in the backend when an attribute is added/edited. It should return only
     * the ADDITIONAL fields and/or markup that are specific for your subclass. The basic form elements
     * are already added and handled automatically.
     * @param form The add/edit form for the attribute.
     * @return The additional Html that you want to inject into the already prepared form.
     */
    public Html editAttributeForm(play.data.Form form){
        return new Html("");
    }

    /**
     * This method is called in the backend when the user edits a Block. This method will be called
     * for each attribute in the Block's assigned AttributeSet. It must return Html containing a field
     * corresponding to the attribute.
     * @param form The full add/edit form for the attribute
     * @param value The selected value of the attribute for the current Block
     * @return The additional Html that should be added to the form, corresponding to the current attribute.
     */
    public abstract Html editForm(play.data.Form form, Value value);

    /**
     * @return The default value for this attribute if no value is set.
     */
    public abstract Value getDefaultValue();

    /**
     * This method will be called when a Block is added/edited in the backend after the user submits
     * the form. The method will be called for each Attribute in the AttributeSet belonging to the Block.
     * The method should return a fully populated instance of a Value, corresponding to what the user
     * has entered in the form for this attribute. You can use for example DynamicForm's bindFromRequest
     * to parse the current request.
     * @return A fully populated Value, corresponding to the request.
     */
    public abstract Value restoreValueFromRequest(Value oldValue);

    /**
     * The AttributeController delegates to this method when the user submits the form for creation
     * of a new Attribute. The default implementation of the method binds the request to a Form<> of
     * the specific Attribute. You can override this implementation if you need to handle the form
     * submission request in any specific or custom way.
     * @return A redirect to the page that should be visible after the user creates a new attribute.
     */
    public Result doAdd() {
        Form form = SmartForm.form(getClass());
        form = form.bindFromRequest();
        if (form.hasErrors()) {
            return Results.badRequest(attributeAddEdit.render(form, this));
        }
        ((Attribute)form.get()).save();
        return Results.redirect(ch.insign.cms.attributeset.controller.routes.AttributeController.index());
    }

    /**
     * The AttributeController delegates to this method when the user submits the form for editing
     * of an existing Attribute. The default implementation of the method binds the request to a Form<>
     * of the specific Attribute. You can override this implementation if you need to handle the form
     * submission request in any specific or custom way.
     * @return A redirect to the page that should be visible after the user edits an existing attribute.
     */
    public Result doEdit(){
        Form form = SmartForm.form((Class<Attribute>)getClass()).fill(this).bindFromRequest();
        if (form.hasErrors()) {
            return Results.badRequest(attributeAddEdit.render(form, this));
        }
        ((Attribute)form.get()).save();
        return Results.redirect(ch.insign.cms.attributeset.controller.routes.AttributeController.index());
    }

    public static class AttributeFinder extends Model.Finder<Attribute>{
        private AttributeFinder(){
            super(Attribute.class);
        }

        public Attribute byKey(String key){
            try {
                return query().andEquals("key", key).getSingleResult();
            } catch(NoResultException e){
                return null;
            }
        }
    }

    // ======== Getters and Setters =========

    public MString getName() {
        return name;
    }

    public void setName(MString name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
