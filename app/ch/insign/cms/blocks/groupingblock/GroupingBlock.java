/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.groupingblock;

import ch.insign.cms.blocks.backendlinkblock.BackendMenuItem;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.NavigationItem;
import ch.insign.cms.models.PageBlock;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *  The Grouping Block does not have any frontend template, it is only used as root node to group together
 *  pages. Typically it has a key which can be used in the template's navigation sections as root navigation element.
 *
 *  Note it also does not have urls and calls to getURL() will return an empty string.
 */
@Entity
@Table(name = "cms_block_grouping")
@DiscriminatorValue("GroupingBlock")
public class GroupingBlock extends PageBlock implements BackendMenuItem {
	private final static Logger logger = LoggerFactory.getLogger(GroupingBlock.class);

    public static BlockFinder<GroupingBlock> find = new BlockFinder<>(GroupingBlock.class);

    private String linkIcon;

    /**
     * Grouping blocks have no concept of visibility, so they're always visible.
     */
    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

	/**
	 * The grouping block does explicitly set an empty ("") url since it's not a link.
	 * (and if rendered in a <a href=""> it will remain empty)
	 */
    @Override
    public Optional<String> onGetUrl(NavigationItem navItem) {
        return Optional.of("");
    }

    @Override
    public Html editForm(Form editForm) {
        return ch.insign.cms.blocks.groupingblock.html.groupingBlockEdit.render(this, (Form<GroupingBlock>)editForm, Controller.request().getQueryString("backURL"), null);
    }

    @Override
    public Html render() {
        logger.error("A GroupingBlock's render method should not be called (GroupingBlocks are only used for navigation structure). Block: ", this);
        return Html.apply("");
    }


    @Override
    public AbstractBlock.BlockType getType() {
        return BlockType.PAGE;
    }

    public String getLinkIcon() {
        return linkIcon;
    }

    public void setLinkIcon(String linkIcon) {
        this.linkIcon = linkIcon;
    }
}
