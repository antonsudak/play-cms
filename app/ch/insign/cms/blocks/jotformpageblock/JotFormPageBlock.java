/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.jotformpageblock;

import ch.insign.cms.blocks.jotformpageblock.model.JotForm;
import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.cms.blocks.jotformpageblock.view.html.preview;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.PageBlock;
import ch.insign.commons.db.MString;
import ch.insign.commons.db.Model;
import ch.insign.commons.i18n.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.twirl.api.Html;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Optional;

@Entity
@Table(name = "cms_page_block_jotform")
@DiscriminatorValue("JotForm")
@NamedQueries({
        @NamedQuery(
                name="JotFormPageBlock.allPages",
                query="SELECT DISTINCT j FROM JotFormPageBlock j"),
        @NamedQuery(
                name="JotFormPageBlock.byApiJotFormId",
                query="SELECT DISTINCT j FROM JotFormPageBlock j INNER JOIN MString s ON j.jotFormId = s LEFT JOIN j.jotFormId t, IN (t.translations) t9n WHERE  t9n LIKE :apiFormId")
})
public class JotFormPageBlock extends PageBlock implements Serializable {

	private final static Logger logger = LoggerFactory.getLogger(JotFormPageBlock.class);

	public static JotFormPageBlockFinder find = new JotFormPageBlockFinder();

    @MString.MStringRequiredForVisibleTab
    @OneToOne(cascade=CascadeType.ALL)
    private MString jotFormId = new MString();

    //Need for synchronisation the party account with the associated email address
    private boolean syncEmail;

    public boolean isSyncEmail() {
        return syncEmail;
    }

    public void setSyncEmail(boolean syncEmail) {
        this.syncEmail = syncEmail;
    }


    public MString getJotFormId() {
        return jotFormId;
    }

    public void setJotFormId(MString jotFormId) {
        this.jotFormId = jotFormId;
    }

    public Html render() {


        Long formId = null;

        JotForm jotForm = JotForm.find.byUid(Long.valueOf(getJotFormId().get(Language.getCurrentLanguage())));

        formId = Optional.ofNullable(jotForm).map(JotForm::getApiFormId).orElse(null);

        if (jotForm == null || !JotFormService.validResponseCode(JotFormService.getAPIform(formId))) {
            logger.error(Messages.get("jotform.no.valid.error", formId));
            return CMS.getJotFormService().render(preview.render(null), this);
        }

        return CMS.getJotFormService().render(preview.render(formId), this);
	}

	@Override
	public Html editForm(Form editForm) {
		return ch.insign.cms.blocks.jotformpageblock.html.jotFormEdit.render(this, editForm, Controller.request().getQueryString("backURL"), null);
	}

	public static class JotFormPageBlockFinder extends Model.Finder<JotFormPageBlock> {
		private JotFormPageBlockFinder() {
			super(JotFormPageBlock.class);
		}

		public TypedQuery<JotFormPageBlock>  byApiJotFormId(Long formId) {
            return jpaApi.em().createNamedQuery("JotFormPageBlock.byApiJotFormId", getEntityClass()).setParameter("apiFormId", "%" + String.valueOf(formId) + "%");
		}

        public TypedQuery<JotFormPageBlock> allPages() {
            return jpaApi.em().createNamedQuery("JotFormPageBlock.allPages", getEntityClass());
        }
	}

    @PostPersist
    @PostUpdate
    private void addSubmissionsWebHook() {
        Long id = Long.valueOf(jotFormId.get(Language.getCurrentLanguage()));
		if (id == null) {
			logger.error("JotFormPageBlock getApiFormId empty");
			return;
		}

    	if (syncEmail && ch.insign.commons.util.Configuration.getOrElse("jotform.api.synchronization.user.email.by.webhook", false)) {
			CMS.getJotFormService().setupJotFormSubmissionsWebHook(id);
		}
	}
}
