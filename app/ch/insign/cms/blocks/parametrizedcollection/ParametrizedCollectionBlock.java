/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.parametrizedcollection;

import ch.insign.cms.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.format.Formats;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Entity
@Table(name = "cms_block_parametrizedcollectionblock")
@DiscriminatorValue("ParametrizedCollectionBlock")
public class ParametrizedCollectionBlock extends CollectionBlock {

    public static BlockFinder<ParametrizedCollectionBlock> find = new BlockFinder<>(ParametrizedCollectionBlock.class);

    private static final Logger logger = LoggerFactory.getLogger(ParametrizedCollectionBlock.class);

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromPublishDate;

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toPublishDate;

    /**
     * This overwrites fromPublishDate and toPublishDate if its bigger than 0
     */
    private Integer dateRangeInDays;

    private String typeFilter;

    private Integer maxResults;

    @Override
    public Html render() {
        try {
            List<PageBlock> blocks;
            if (dateRangeInDays != null && dateRangeInDays > 0) {
                // Overwrite fromPublishDate and toPublishDate
                LocalDateTime today = LocalDateTime.now().toLocalDate().atStartOfDay().plusDays(1);
                LocalDateTime from = today.minusDays(dateRangeInDays);
                blocks = new BlockFinder<>(PageBlock.class).byTypeAndDate(typeFilter,
                        Date.from(from.atZone(ZoneId.systemDefault()).toInstant()),
                        Date.from(today.atZone(ZoneId.systemDefault()).toInstant()), maxResults);
            } else {
                blocks = new BlockFinder<>(PageBlock.class).byTypeAndDate(typeFilter, fromPublishDate, toPublishDate, maxResults);
            }

            return ch.insign.cms.blocks.parametrizedcollection.views.html.parametrizedCollection.render(this, blocks);
        } catch (Exception e) {
            logger.error("Error rendering ParametrizedCollectionBlock", e);
        }
        return ch.insign.cms.blocks.parametrizedcollection.views.html.parametrizedCollection.render(this,
                new ArrayList<>(0));
    }


    @Override
    public Html editForm(Form editForm) {
        return ch.insign.cms.blocks.parametrizedcollection.views.html.parametrizedCollectionEdit.render(
                this,
                editForm,
                Controller.request().getQueryString("backURL"),
                null);
    }

    /*
     * We don't have the option to add child blocks because the added blocks might be invisible (inside this block)
     * and they are not displayed in either the frontend or backend navigation.
     * One option was to extend from GroupingBlock to add a navigation item in the backend. But this only works if
     * the tree up to page is a grouping block
     */
    @Override
    public List<Class<? extends AbstractBlock>> getAllowedSubBlocks() {
        return new ArrayList<>();
    }

    public Map<String, String> options() {
        Map<String, String> values = new HashMap<>();
        CMS.getBlockManager().getBlocks(BlockType.PAGE).stream()
                .filter(clazz -> !clazz.equals(ParametrizedCollectionBlock.class))
                .filter(clazz -> !clazz.equals(getMyPage().getClass()))
                .filter(clazz -> clazz.isAnnotationPresent(DiscriminatorValue.class))
                .forEach(clazz -> {
                    DiscriminatorValue discriminator = clazz.getAnnotation(DiscriminatorValue.class);
                    values.put(discriminator.value(), clazz.getSimpleName());});
        return values;
    }

    public Date getFromPublishDate() {
        return fromPublishDate;
    }

    public void setFromPublishDate(Date fromPublishDate) {
        this.fromPublishDate = fromPublishDate;
    }

    public Date getToPublishDate() {
        return toPublishDate;
    }

    public void setToPublishDate(Date toPublishDate) {
        this.toPublishDate = toPublishDate;
    }

    public String getTypeFilter() {
        return typeFilter;
    }

    public void setTypeFilter(String typeFilter) {
        this.typeFilter = typeFilter;
    }

    public Integer getDateRangeInDays() {
        return dateRangeInDays;
    }

    public void setDateRangeInDays(Integer dateRangeInDays) {
        this.dateRangeInDays = dateRangeInDays;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }
}
