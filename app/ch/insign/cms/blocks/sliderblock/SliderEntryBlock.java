/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.sliderblock;

import ch.insign.cms.models.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.MoxiemanagerConfiguration;
import ch.insign.cms.utils.MoxieThumbnail;
import ch.insign.commons.db.MString;
import play.twirl.api.Html;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;


import javax.persistence.*;
import java.io.File;

@Entity
@Table(name = "cms_block_slider_entry")
@DiscriminatorValue("SliderEntryBlock")
public class SliderEntryBlock extends AbstractBlock  {
	private final static Logger logger = LoggerFactory.getLogger(SliderEntryBlock.class);

    @OneToOne(cascade= CascadeType.ALL)
    private MString title = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString text = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString link = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString linkText = new MString();

    private boolean linkInNewWindow = false;

    @OneToOne (cascade=CascadeType.ALL)
    private MString background = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString image = new MString();

    @Constraints.Required
    private int imageMaxWidth = 260;

    @Constraints.Required
    private int imageMaxHeight = 340;

    @Constraints.Required
    private double imageQuality = 0.75;

    @Override
    public AbstractBlock.BlockType getType() {
        return BlockType.BLOCK;
    }

    @Override
    public Html render() {
        return ch.insign.cms.blocks.sliderblock.html.sliderEntryBlockShow.render(this);
    }

    @Override
    public Html editForm(Form editForm) {
        return ch.insign.cms.blocks.sliderblock.html.sliderEntryBlockEdit.render(
                this,
                (Form<SliderEntryBlock>) editForm,
                Controller.request().getQueryString("backURL"));
    }

    @Override
    public void save() {
        createThumbnailsForImage(image, imageMaxWidth, imageMaxHeight, imageQuality);
        super.save();
    }

    public MString getTitle() {
        return title;
    }

    public void setTitle(MString title) {
        this.title = title;
    }

    public MString getText() {
        return text;
    }

    public void setText(MString text) {
        this.text = text;
    }

    public MString getLink() {
        return link;
    }

    public void setLink(MString link) {
        this.link = link;
    }

    public MString getLinkText() {
        return linkText;
    }

    public void setLinkText(MString linkText) {
        this.linkText = linkText;
    }

    public MString getBackground() {
        return background;
    }

    public void setBackground(MString background) {
        this.background = background;
    }

    public MString getImage() {
        return image;
    }

    public void setImage(MString image) {
        this.image = image;
    }

    public boolean isLinkInNewWindow() {
        return linkInNewWindow;
    }

    public void setLinkInNewWindow(boolean linkInNewWindow) {
        this.linkInNewWindow = linkInNewWindow;
    }

    public int getImageMaxWidth() {
        return imageMaxWidth;
    }

    public void setImageMaxWidth(int imageMaxWidth) {
        this.imageMaxWidth = imageMaxWidth;
    }

    public int getImageMaxHeight() {
        return imageMaxHeight;
    }

    public void setImageMaxHeight(int imageMaxHeight) {
        this.imageMaxHeight = imageMaxHeight;
    }

    public double getImageQuality() {
        return imageQuality;
    }

    public void setImageQuality(double imageQuality) {
        this.imageQuality = imageQuality;
    }

    private void createThumbnailsForImage(MString image, int widthImage, int heightImage, double qualityImage) {
        if (image == null) {
            return;
        }

        for (String lang : CMS.getConfig().frontendLanguages()) {
            if (image.get(lang).equals("")) {
                continue;
            }
            try {
                image.set(lang, MoxieThumbnail.createThumbnail(image.get(lang),
                        widthImage, heightImage, qualityImage));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
