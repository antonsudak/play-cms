/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.reviewblock.forms;

import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

public class ReviewSubmissionForm {

	@Constraints.Required
	protected String displayName;

	protected String answer;

	protected Integer rating;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<>();

		// js star plugin always pass "0" if no rating was selected
		if (rating < 1 || rating > 5) {
			rating = null;
		}
		if (null == rating && StringUtils.isEmpty(answer)) {
			errors.add(new ValidationError("answer", Messages.get("frontend.review.form.answer_or_rating_required")));
		}
		return errors.isEmpty() ? null : errors;
	}
}
