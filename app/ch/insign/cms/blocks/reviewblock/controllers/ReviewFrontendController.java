/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.reviewblock.controllers;

import ch.insign.cms.blocks.reviewblock.forms.ReviewSubmissionForm;
import ch.insign.cms.blocks.reviewblock.models.ReviewBlock;
import ch.insign.cms.blocks.reviewblock.models.ReviewSubmission;
import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.email.EmailService;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.PageBlock;
import ch.insign.commons.db.Paginate;
import ch.insign.commons.db.SmartForm;
import ch.insign.commons.i18n.Language;
import ch.insign.commons.util.Configuration;
import ch.insign.playauth.PlayAuth;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.primitives.Ints;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

@With(GlobalActionWrapper.class)
@Transactional
public class ReviewFrontendController extends Controller {
	private final static Logger logger = LoggerFactory.getLogger(ReviewFrontendController.class);

    public static final String REVIEW_SESSION_KEY = "review_uid";
	public static final String COOKIE_DISPLAY_NAME_KEY = "review_display_name";
	public enum Sorting {
		THUMBS_UP, DATE
	}

	private final MessagesApi messagesApi;

    private final EmailService emailService;

	@Inject
	public ReviewFrontendController(MessagesApi messagesApi, EmailService emailService) {
		this.messagesApi = messagesApi;
        this.emailService = emailService;
    }

	public Result list(String reviewBlockId, int page, String sortBy) {
		ObjectNode result = Json.newObject();
		ReviewBlock reviewBlock = ReviewBlock.find.byId(reviewBlockId);

		if (null == reviewBlock) {
			result.put("status", "error");
			result.put("message", messagesApi.get(lang(), "frontend.review.not_found.label"));
			return ok(result);
		}

		Sorting sorting = Sorting.THUMBS_UP;
		try {
			sorting = Sorting.valueOf(sortBy);
		} catch (IllegalArgumentException e) {
			logger.warn("Wrong sortBy parameter recieved. Using default one.");
		}

		F.Tuple<TypedQuery<ReviewSubmission>, Long> submissionsResult = ReviewSubmission.find.byReview(reviewBlock, sorting);
		Paginate<ReviewSubmission> paginate = new Paginate<>(submissionsResult._1, CMS.getConfig().getReviewSubmissionsCount(), Ints.checkedCast(submissionsResult._2));

		result.put("status", "ok");
		result.put("content", ch.insign.cms.blocks.reviewblock.view.block.html.listSubmissions.render(paginate, page).toString());
		result.put("currentPage", page);
		result.put("totalPages", paginate.getTotalPageCount());
		result.put("hasMorePages", paginate.getTotalPageCount() > page);
		return ok(result);
	}


	public Result addSubmission(String reviewBlockId) {
		ObjectNode result = Json.newObject();
		ReviewBlock reviewBlock = ReviewBlock.find.byId(reviewBlockId);

		if (null == reviewBlock) {
			result.put("status", "error");
			result.put("message", messagesApi.get(lang(), "frontend.review.not_found.label"));
			return ok(result);
		}

		if (!canAddReview(reviewBlock)) {
			result.put("status", "error");
			result.put("message", messagesApi.get(lang(), "frontend.review.add.not_allowed"));
			return ok(result);
		}

		Form<ReviewSubmissionForm> form = SmartForm
				.form(ReviewSubmissionForm.class)
				.bindFromRequest();

		if (form.hasErrors()) {
			result.put("status", "error");
			result.set("errors", form.errorsAsJson());
			return ok(result);
		}

		ReviewSubmission reviewSubmission = new ReviewSubmission();
		reviewSubmission.setReview(reviewBlock);
		reviewSubmission.setRating(form.get().getRating());
		reviewSubmission.setDisplayName(form.get().getDisplayName());
		reviewSubmission.setAnswer(form.get().getAnswer());
		reviewSubmission.setUid(getOrCreateUid());

		if (PlayAuth.isAuthenticated()) {
			reviewSubmission.setEmail(PlayAuth.getCurrentParty().getEmail());
		}
		reviewBlock.addSubmission(reviewSubmission);
		reviewSubmission.save();

		if (StringUtils.isBlank(reviewSubmission.getAnswer())) {
			reviewSubmission.approve();
			result.put("reviewStatus", "approved");
		} else {
			result.put("reviewStatus", "waiting-for-aprove");
		}

		result.put("status", "ok");
		result.put("submissionId", reviewSubmission.getId());

		reviewBlock.markModified();

		saveNameInCookie(reviewSubmission.getDisplayName());
		sendNewSubmissionNotification(reviewSubmission);

		return ok(result);
	}

	/**
	 * Provides ability for the user to add rating to his own review later if no rating was passed earlier
	 *
	 * @param submissionId
	 * @param submissionRating Rating value between 1 and 5
	 * @return
	 */
	public Result addRatingToOldSubmission(String submissionId, Integer submissionRating) {
		ObjectNode result = Json.newObject();
		ReviewSubmission reviewSubmission = ReviewSubmission.find.byId(submissionId);
		if (!canAddRatingToOldReview(reviewSubmission)
				|| submissionRating < 1 || submissionRating > 5) {
			result.put("status", "error");
			return ok(result);
		}

		reviewSubmission.setRating(submissionRating);
		result.put("status", "ok");
		return ok(result);
	}

	/**
	 * @param reviewSubmission
	 * @return True if the current user is the owner of the provided submission,
	 * and the submission doesn't have rating yet
	 */
	public static boolean canAddRatingToOldReview(ReviewSubmission reviewSubmission) {
		return null != reviewSubmission
				&& reviewSubmission.getRating() == null
				&& reviewSubmission.getUid().equals(getOrCreateUid());
	}

	/**
	 * Save display name in long cookie to use it later
	 * @param name
	 */
	public void saveNameInCookie(String name) {
		try {
			response().setCookie(new Http.Cookie(
					COOKIE_DISPLAY_NAME_KEY,
					URLEncoder.encode(name, "UTF-8"),
					60 * 60 * 24 * 30 * 6,  // max age 6 months
					"/",
					"",
					false,
					true
			));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get display name of previously saved review submission
	 * @return Optional of display name
	 */
	public static Optional<String> getNameFromCookie() {
		return Optional.ofNullable(request().cookie(COOKIE_DISPLAY_NAME_KEY))
				.map(c -> c.value())
				.map(value -> {
					try {
						return URLDecoder.decode(value, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						return null;
					}
				});
	}

	/**
	 * Provides ability to change submission's own rating
	 * (Thumb's up/down click handling)
	 *
	 * @param submissionId The id of the submission
	 * @param submissionRating Rating value, 1 or -1 depending on thumbs-up or thumbs-down click
	 * @return
	 */
	public Result changeRating(String submissionId, Integer submissionRating) {
		ObjectNode result = Json.newObject();
		ReviewSubmission reviewSubmission = ReviewSubmission.find.byId(submissionId);
		if (null == reviewSubmission || !reviewSubmission.isApproved()) {
			return notFound("Submission was not found or is not approved by admin");
		}

		// "1" and "-1" for thumbsUp and thumbsDown
		if (submissionRating != -1 && submissionRating != 1) {
			return badRequest();
		}

		String uid = getOrCreateUid();
		Integer currentRating = reviewSubmission.getAnswerRating(uid);
		if (null == currentRating) {
			reviewSubmission.addRating(uid, submissionRating);
		} else if (!currentRating.equals(submissionRating)) {
			reviewSubmission.removeRating(uid);
			reviewSubmission.addRating(uid, submissionRating);
		} else if (currentRating.equals(submissionRating)) {
			reviewSubmission.removeRating(uid);
		}

		result.put("status", "ok");
		result.put("answerRating", reviewSubmission.getAnswerRating());
		return ok(result);
	}

	public static boolean canAddReview(ReviewBlock reviewBlock) {
		if (!ReviewBlock.ReviewStatus.ENABLED.equals(reviewBlock.getStatus())) {
			return false;
		}

		if (reviewBlock.isLoginRequired() && !PlayAuth.isAuthenticated()) {
			return false;
		}

		String uid = getOrCreateUid();
		try {
			if (null != ReviewSubmission.find.byReviewAndUid(reviewBlock, uid)) {
				return false;
			}
		} catch (NonUniqueResultException e) {
			return false;
		}

		return true;
	}

	public static String getOrCreateUid() {
		String uid;
		if (PlayAuth.isAuthenticated()) {
			uid = PlayAuth.getCurrentParty().getId();
		} else {
			uid = Optional.ofNullable(session(REVIEW_SESSION_KEY))
					.orElse(UUID.randomUUID().toString());
		}

		session(REVIEW_SESSION_KEY, uid);
		return uid;
	}

	/**
	 * Send new submission notification to the email specified in config
	 * @param submission New submision
	 */
	private void sendNewSubmissionNotification(ReviewSubmission submission) {
		if (!Configuration.getOrElse("cms.reviewblock.newSubmission.notification.enabled", false)) {
			return;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

		HashMap<String, String> emailData = new HashMap<>();
		emailData.put("reviewEmail", submission.getEmail());
		emailData.put("reviewDisplayName", submission.getDisplayName());
		emailData.put("reviewTitle", submission.getReview()
				.getTargetObject()
				.filter(b -> b instanceof PageBlock)
				.map(b -> ((PageBlock) b).getPageTitle().get())
				.orElse(submission.getReview().getTitle().get()));
		emailData.put("reviewDate", sdf.format(submission.getCreatedOn()));
		emailData.put("reviewText", submission.getAnswer());
		emailData.put("reviewRating", Optional.ofNullable(submission.getRating())
				.map(Object::toString)
				.orElse(" - ")
		);
		emailData.put("reviewUrl",
				ch.insign.cms.blocks.reviewblock.controllers.routes.ReviewBackendController.list(submission.getReview().getTargetId().toString(), 1, 0)
						.absoluteURL(Http.Context.current().request(), true)
		);

		emailService.send(
				"reviews.new_submission",
				Configuration.getOrElse("cms.reviewblock.newSubmission.notification.email",""),
				emailData,
				Language.getDefaultLanguage()
		);
	}
}
