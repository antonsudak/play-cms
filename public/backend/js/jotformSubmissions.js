$(function () {

    $('#jot-form-success-update').hide();

    jotFormDataTable();
    jotFormsFilter();
    jotFormsExport();
    jotFormsSyncSubmissions();
    jotFormsLanguage();

});

function jotFormDataTable() {
    if (!jQuery().dataTable) {
        $("#loader-submissions-table").hide();
        return;
    }

    $.getJSON($('#datatableUrl').val(), function (data) {
        if (data === null
            || typeof data.aaData === "undefined"
            || data.aaData === null
            || typeof data.aoColumns === "undefined"
            || data.aoColumns === null
        ) {
            $("#loader-submissions-table").hide();
            return;
        }


        jsconfig.datatables.bServerSide = false;
        jsconfig.datatables.aaData =  data.aaData;
        jsconfig.datatables.aoColumns = data.aoColumns;
        jsconfig.datatables.bPaginate = true;
        jsconfig.datatables.bJQueryUI = true;
        jsconfig.datatables.iDisplayLength = 20;

        oTable = $('#jot-form-submissions').dataTable(jsconfig.datatables);

        jQuery('#jot-form-submissions_wrapper .dataTables_length select').addClass("form-control input-small ");
        jQuery('#jot-form-submissions_wrapper .dataTables_length select').css({display: "block"});
        jQuery('#jot-form-submissions_wrapper .dataTables_length').addClass("pull-right");
        $("#loader-submissions-table").hide();
    });
}

function jotFormsFilter() {
    var $jotFormsFilter = $("#jotFormsFilter");
    if (typeof $jotFormsFilter !== "undefined") {
        $jotFormsFilter.change(function (e) {
            e.preventDefault();
            window.location = $(this).children('option:selected').data('url');
            return false;
        });
    }
}

function jotFormsLanguage() {
    var jotFormslanguage = $("#jotFormslanguage");
    if (typeof jotFormslanguage !== "undefined") {
        jotFormslanguage.change(function (e) {
            e.preventDefault();
            window.location = $(this).children('option:selected').data('url');
            return false;
        });
    }
}

function jotFormsExport() {
    $("#jotFormExport").click(function () {
        $(this).attr('href', $(this).data('url'));
    });
}

function jotFormsSyncSubmissions() {
    $("#updateSubmissions").click(function () {
        $.get($(this).data('url'));
        $('#jot-form-success-update').show();
    });
}

