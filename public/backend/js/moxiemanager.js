function cleanInput(args, element, errorMessage) {
    var domains = ['http://ftp.ringier.ch', 'https://ftp.ringier.ch'];
	var currentValue = $(element).val();
	var newValue = args.focusedFile.meta.url;
    var hasError = true;

    $.each(domains, function(key, domain) {
        if (newValue.substring(0, domain.length) == domain) {
            args.focusedFile.url = newValue.substring(domain.length);
            hasError = false;
        }
    });

    // if no occurence of known domains found then there is an error
    if (hasError) {
        args.focusedFile.url = currentValue;
        alert(errorMessage);
    }
}

// edit field should support both ftp and manual upload
function cleanInputWithoutValidation(args, element, errorMessage) {
	var domains = ['http://ftp.ringier.ch', 'https://ftp.ringier.ch'];
	var currentValue = $(element).val();
	var newValue = args.focusedFile.meta.url;

    $.each(domains, function(key, domain) {
        if (newValue.substring(0, domain.length) == domain) {
            args.focusedFile.url = newValue.substring(domain.length);
        }
    });
}